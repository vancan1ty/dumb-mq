;;; &Copyright: Currell Berry, 2017
;;; this folder contains the implementation of the berrysync "client" program
;;; this program runs on the "node" devices, sends updates up to the central server
;;; and listens for changes locally

(in-package :berrysync)

(declaim (optimize (speed 0) (debug 3) (safety 3)))

(defvar *server-host*)
(defvar *server-port*)
(defvar *folder-id-to-path-map*)
(load "berrysync-config.lisp")

(defclass dmq-client-config (dmq-config)
  ())

(defparameter *listen-for-events-from-hub-thread* nil)
(defparameter *listen-for-events-from-hub-port* 9000)

(defparameter *keepalive-querier-thread* nil)
(defparameter *watch-for-folder-changes-thread* nil)
(defparameter *seconds-to-sleep-between-folder-scans* 120)

(defparameter *listen-for-events-changes-thread* nil)
(defparameter *listen-for-events-changes-port* 9001)

(defparameter *server-to-node-socket-send-lock* (bt:make-lock))
(defparameter *server-to-node-socket* nil)

(defparameter *password-timeout-millis* 4000)
(defparameter *successive-push-failures* 0)

(defun berrysync-start-node ()
  "this is the thing that runs on the client computers and waits for changes
       either in the folder itself
       or upstream from the hub-server
   if a change happens within the folder then we
          [first must add all the files]
       1. commit the change
       2. push to the server.  if the push fails then we
          a. pull and merge with the current version on the hub server
          b. check to see whether the merge completed successfully
            if it did then we are good to go
            if it did then we desktop notify the user and launch a merge resolver on the user's screen
          c. push to the hub server.  if that fails, we go back to (a)

   the hub server notifies the client server of a change by using the
       'c' command
        if the client receives this command on a line by its own, then it
        1. commits any working tree changes
        2. pulls from the remote server
        3. checks whether the merge completed successfully.
             if it did then we are good to go for now
             if it did then we desktop notify the user and launch a merge resolver on the user's screen
        4. if there are unpushed changes then we now push (going to section one (a) above)
"
  (start-listen-for-events-from-hub)
  (start-keepalive-querier)
  (start-watch-for-folder-change)
)

(defun start-listen-for-events-from-hub ()
  (setf *listen-for-events-from-hub-thread* (bt:make-thread
                                             (lambda () (listen-for-event-from-hub)))))

(defun start-keepalive-querier ()
  (setf *keepalive-querier-thread* (bt:make-thread
                                    (lambda () (run-keepalive-querier)))))


;; one of the goals is -- on save commit

;;(defparameter *keepalive-sleep-seconds* (* 60 40))
(defparameter *keepalive-sleep-seconds* (* 60 4))
(defparameter *last-hub-communication-time* 0)

(defun keepalive-reply (socket)
  (bt:with-lock-held (*server-to-node-socket-send-lock*)
    (format t "INFO: sending keepalive reply to ~a~%" socket)
    (format (usocket:socket-stream socket) "a~%")
    (force-output (usocket:socket-stream socket))))

(defun keepalive-query (socket)
  (bt:with-lock-held (*server-to-node-socket-send-lock*)
    (format t "INFO: sending keepalive query to ~a~%" socket)
    (format (usocket:socket-stream socket) "k~%")
    (force-output (usocket:socket-stream socket))))

(lefun notify-server-of-change (socket folder-id)
  (assert (not (or (eql nil socket) (eql nil folder-id))))
  (bt:with-lock-held (*server-to-node-socket-send-lock*)
    (format t "INFO: sending change notification to ~a~%" socket)
    (format (usocket:socket-stream socket) "c ~a~%" folder-id)
    (force-output (usocket:socket-stream socket))))

(defun run-keepalive-querier ()
  (let (current-time time-difference-ms)
    (do () (())
      (sleep *keepalive-sleep-seconds*)
      (setq current-time (get-internal-real-time))
      (setq time-difference-ms (- current-time *last-hub-communication-time*))
      (when (> (* 1000 *keepalive-sleep-seconds*) time-difference-ms) ;;time to send out the keepalive
        (keepalive-query *server-to-node-socket*)))))

(defun listen-for-event-from-hub ()
  (let (mstream) 
    (loop while t do
         (block continue
           (setf *server-to-node-socket* (usocket:socket-connect *server-host* *server-port*))
           (setq mstream (usocket:socket-stream *server-to-node-socket*))
           (unwind-protect
                (loop while t do
                     (let (folder-id line-read command)
                       (setq line-read (read-line mstream))
                       (format t "INFO: read line ~s from ~a~%" line-read *server-host*)
                       (when (eql (length line-read) 0) ;;then I believe this means that the connection is closed
                         (progn
                           (format t "Connection shut down by server ~a~%" (usocket:get-peer-address *server-to-node-socket*))
                           (return-from continue)))
                       (setq *last-hub-communication-time* (get-internal-real-time))
                       (setq command (elt line-read 0))
                       (cond 
                         ((eql command #\c)
                          (setf folder-id (subseq line-read 2))
                          (on-change-notification-from-hub (gethash folder-id *folder-id-to-path-map*)))
                         ((eql command #\k)
                          (keepalive-reply *server-to-node-socket*))
                         ((eql command #\a)
                          nil) ;;nothing further to do, this is the keepalive acknowledgement
                         (t
                          (format *error-output* "unsupported command ~a~%" command)))))
             (progn
               (format t "WARN: closing *server-to-node-socket*")
               (usocket:socket-close *server-to-node-socket*)))))))

;; the below is signalled when something which we don't know how to recover from happens
;; ideally we should backtalk to the server to let the server know
(define-condition berrysync-error 
    (simple-error) () (:documentation "whenever we get this error we should try to prompt the user"))


(lefun on-change-notification-from-hub (folder)
  "called after a we receive a change notification from the server for a folder that we listen on"
  (assert(not (eql nil folder)))
  (let (commit-result-code commit-result-text nresultkey push-result-indicator)
    (setf (values commit-result-code commit-result-text)
          (run-git folder (list "commit" "-a" "--message" (concatenate 'string "berrysync change detected on " (machine-instance)))))
    (when (not (eql commit-result-code 0))
      (if (search "nothing to commit" commit-result-text );then there is no error
          nil
          (signal (make-condition 'berrysync-error :format-control "failure to commit ~s" :format-arguments (list commit-result-code)))))
    (setf nresultkey (git-fetch-merge-push folder))
    (cond
      ((eql nresultkey :success)
       nil ) ;;continue onwards
      ((eql nresultkey :merge-failure)
       (launch-merge-assistant folder))
      (t
       (signal (make-condition 'berrysync-error :format-control "failure to fetch and merge ~a" :format-arguments (list nresultkey)))))
    (setf push-result-indicator (git-push folder))
    (cond
      ((eql push-result-indicator :success)
       (progn (setf *successive-push-failures* 0) :success))
      ((eql push-result-indicator :failure)
       (progn
         (incf *successive-push-failures*)
         )))))

(defun start-watch-for-folder-change ()
    (setf *watch-for-folder-changes-thread* (bt:make-thread
                                             (lambda () (watch-for-folder-change)))))

(defparameter *time-to-sleep-between-folder-scans* 300)

(defun watch-for-folder-change ()
  (loop while t do
       (maphash
        (lambda (folder-id folder-path)
          (let (log-exit-code log-exit-data status-exit-code status-exit-data)
            (when (> *successive-push-failures* 5) ;;no point in consuming resources
              (format t "INFO: sleeping due to *successive-push-failures*=~a~%" *successive-push-failures*)
              (sleep *time-to-sleep-between-folder-scans*))
            (handler-case 
                (progn
                  (setf (values log-exit-code log-exit-data)
                        (run-git folder-path (list "log" "--oneline"
                                                   "origin/master..master")))
                  (format t "INFO: git pretty log returned ~a and ~a~%"
                          log-exit-code log-exit-data)
                  (when (not (eql log-exit-code 0))
                    (signal (make-condition 'berrysync-error
                                            :format-control "git log failure ~s,~s"
                                            :format-arguments log-exit-code log-exit-data)))
                  (setf (values status-exit-code status-exit-data)
                        (run-git folder-path (list "status" "--porcelain")))
                  (format t "INFO: git status returned ~a and ~a~%" status-exit-code status-exit-data)
                  (when (not (eql status-exit-code 0))
                    (signal (make-condition 'berrysync-error
                                            :format-control "git status failure ~s,~s"
                                            :format-arguments status-exit-code status-exit-data)))
                  (when (or (> (length log-exit-data) 1) (> (length status-exit-data) 1))
                                        ;then there are modified files of some type!
                    (on-folder-change folder-id folder-path))
                  (sleep *seconds-to-sleep-between-folder-scans*))
            (berrysync-error (condition)
                             (format t "ERROR:wffce1 ~a~%" condition)
                             (launch-merge-assistant folder-path))
            (error (condition)
                (SB-DEBUG:PRINT-BACKTRACE) 
                (format t "ERROR:wffce2 ~a~%" condition)
                (sleep 1)))))
        *folder-id-to-path-map*
        )))
;(apropos :backtrace)
;(documentation #'sb-debug:print-backtrace)

;(sb-di:

;; (macroexpand
;;  (berryutils:lefun foo (bar baz)
;;    "hello there"
;;    (declare (optimize (speed 0)))
;;    (+ bar baz)))

;; (foo 2 3)

;; (cdr (list "hello" 1 2 3))

(defmacro docstringdetector (name parameters &body body)
 (format t "see following1: ~s~%" body)
 (when (typep (car body) 'string)
   (format t "doing it!~%")
   (setf body (cdr body))
   nil)
 (format t "see following2: ~s~%" body)
  `(defun ,name ,parameters
     (let ()
       ,@body)))

;; (macroexpand-1 (docstringdetector mynewfun ()
;;   "hello there"
;;   (declare (optimize (debug 3)))
;;   (+ 2 2))sb-di:unknown-debug-var)

;; (cdr ("hello" 1 2))
;; (mynewfun)


(lefun on-folder-change (folder-id folder-path)
  "called after a change is detected inside a folder that we listen on"
  (let (add-result-code add-result-text commit-result-code commit-result-text push-result-indicator f-m-p-result-key)
    (setf (values add-result-code add-result-text)
          (run-git folder-path (list "add" "*")))
    (when (not (eql add-result-code 0))
      (signal (make-condition 'berrysync-error :format-control "unable to add! code: ~s, data: ~s~%" :format-arguments (list add-result-code add-result-text))))


    (setf (values commit-result-code commit-result-text)
          (run-git folder-path (list "commit" "-a" "--message"
                                (concatenate 'string "berrysync change detected on " (machine-instance)))))
    (when (not (eql commit-result-code 0))
      (if (search "nothing to commit" commit-result-text );then there is no error
          nil
          (signal (make-condition 'berrysync-error :format-control "failure to commit. code: ~s, data: ~s~%" :format-arguments (list commit-result-code commit-result-text)))))
    (setf push-result-indicator (git-push folder-path))
    (cond
      ((eql push-result-indicator :success)
       (progn (setf *successive-push-failures* 0) :success))
      ((eql push-result-indicator :failure)
       (progn
         (incf *successive-push-failures*)
         (setq f-m-p-result-key (git-fetch-merge-push folder-path))
         (cond ((eql f-m-p-result-key :success)
                :success)
               ((eql f-m-p-result-key :merge-failure)
                (progn
                  (format t "WARN: :merge-failure~%")
                  (launch-merge-assistant folder-path)
                  :failure))
               (t
                (signal (make-condition 'berrysync-error :format-control "failure to fetch-merge-push ~a" :format-arguments (list f-m-p-result-key))))))))
    (when (or (equal push-result-indicator :success) (equal f-m-p-result-key :success))
      (notify-server-of-change *server-to-node-socket* folder-id))))


(lefun launch-merge-assistant (folder)
  "this blocks while it is invoked!"
  (sb-ext:run-program  "/usr/bin/python" (list "/usr/bin/git-cola") :pty t :directory folder)
  nil)

(defun launch-error-prompt (condition folder)
  "TODOFIX!"
  nil)

;;(sb-ext:run-program "/bin/sh" (list "-c" "git difftool") :pty t :directory (elt *listen-folders-list* 0))

;;;the below two options work -- one pulls up a difftool, the other pulls up git-cola
;;(sb-ext:run-program  "/usr/bin/meld" (list ".") :pty t :directory (elt *listen-folders-list* 0))
;;(sb-ext:run-program  "/usr/bin/python" (list "/usr/bin/git-cola") :pty t :directory (elt *listen-folders-list* 0))

;(launch-merge-assistant (elt *listen-folders-list* 0))

;; there are going to be two sockets
;; (both unencrypted for now, but I would like to figure out some way to secure the server-node connection)
;; one where the node initiates the connection to the hub, and does not accept incoming connections.
;; SECONDARY another where local programs initiate connections to the node to notify the node immediately of changes.


(require :alexandria)

(defparameter *server-host* "cvberry.com")
(defparameter *server-port* 4040)
(defparameter *folder-id-to-path-map*
  (alexandria:plist-hash-table
   '(:bsync-test-1 "/projects/berrysync/testing/repo1")))

(require :alexandria)
(in-package :berrysync-client)

;(defparameter *server-host* "cvberry.com")
(defparameter *server-host* #(127 0 0 1))
(defparameter *server-port* 4040)
(defparameter *folder-id-to-path-map*
  (alexandria:plist-hash-table
   '("bsync-test-1" "/projects/berrysync/testing/repo2") :test 'equal))


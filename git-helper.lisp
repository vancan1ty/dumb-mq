(in-package :berrysync)

(defparameter *successive-push-failures* 0)

;; git function return values
;; :success
;; :failure
;; :error


(lefun git-push (folder)
  "possible return values: :failure :success :error"
  (declare (optimize (debug 3)))
  (let (expect-processed-output-from-server process-obj server-to-us-stream
                                            us-to-server-stream expect-return-code data-read-in-expect)
    (unwind-protect
         (progn
           (setq expect-processed-output-from-server "")
           (setq process-obj (sb-ext:run-program
                              "/usr/bin/git" (list "push" "origin" "master") :wait nil :pty nil
                              :output :stream :input :stream :error :output :directory folder))
           (setq server-to-us-stream (sb-ext:process-output process-obj))
           (setq us-to-server-stream (sb-ext:process-input process-obj))
           (handler-case 
               (progn
                 (setf (values expect-return-code data-read-in-expect)
                       (dumb-expect 4000 server-to-us-stream us-to-server-stream
                                    (make-password-response-provider "nopass")))
                 (sb-ext:process-wait process-obj t)
                 (setf expect-processed-output-from-server
                       (concatenate 'string data-read-in-expect
                                    (read-output-stream-to-string-nonblocking (sb-ext:process-output process-obj))))
                 (format t "INFO: output from server: ~%START~%~a~%END~%" expect-processed-output-from-server)
                 (if (not (eql 0 (sb-ext:process-exit-code process-obj)))
                     (progn
                       (format t "ERROR: git push returned error ~A~%" (sb-ext:process-exit-code process-obj))
                       :failure)
                     (progn
                       (format t "INFO: git push returned success code 0!~%")
                       :success)))
             (simple-error (data)
               (progn
                 (format t "ERROR: simple-error in git-push! ~A ~%" data)
                 :error))
             (error (data)
               (progn
                 (format t "ERROR: error in git-push! ~A ~%" data)
                 (signal data)))
             ))
      (progn
        (when (sb-ext:process-p process-obj)
          (sb-ext:process-close process-obj))
        ))))


(lefun git-fetch (folder)
  "possible return values: :failure :success :error"
  (declare (optimize debug))
  (let (expect-processed-output-from-server process-obj server-to-us-stream us-to-server-stream)
    (unwind-protect
         (progn
           (setq process-obj (sb-ext:run-program  "/usr/bin/git" (list "fetch" "origin" "master") :wait nil :pty nil :output :stream :input :stream :error :output :directory folder))
           (setq server-to-us-stream (sb-ext:process-output process-obj))
           (setq us-to-server-stream (sb-ext:process-input process-obj))
           (handler-case 
               (multiple-value-bind (expect-return-code data-read-in-expect)
                   (dumb-expect 4000 server-to-us-stream us-to-server-stream (make-password-response-provider "nopass"))
                 (declare (ignore expect-return-code))
                 (sb-ext:process-wait process-obj t)
                 (setf expect-processed-output-from-server (concatenate 'string data-read-in-expect (read-output-stream-to-string-nonblocking (sb-ext:process-output process-obj))))
                 (format t "output from server: ~%START~%~a~%END~%" expect-processed-output-from-server)
                 (if (not (eql 0 (sb-ext:process-exit-code process-obj)))
                     (progn
                       (format t "ERROR: git fetch returned error ~A~%" (sb-ext:process-exit-code process-obj))
                       :failure)
                     (progn
                       (format t "git push returned success code 0!~%")
                       :success)))
             (simple-error (data)
               (progn
                 (format t "ERROR!!!! ~A ~%" data)
                 :error))))
      (progn
        (when (sb-ext:process-p process-obj)
          (sb-ext:process-close process-obj))
        ))))

(lefun git-fetch-merge-push (folder)
  ":success, :pull-failure, :merge-failure"
  (let (pullstatus mergestatus mergedata pushstatus)
    (setq pullstatus (git-fetch folder))
    (when (not (eql pullstatus :success))
      (return-from git-fetch-merge-push :pull-failure))
    (setf (values mergestatus mergedata) (run-git folder (list "merge" "origin/master")))
    (when (not (eql 0 mergestatus))
      (format t "merge failure ~s,~s~%" mergestatus mergedata)
      (return-from git-fetch-merge-push :merge-failure))
    (setf pushstatus (git-push folder))
    pushstatus))

(lefun run-git (directory argslist)
  "returns process exit code and the output stream results as a string"
  (assert (not (eql directory nil)))
  (let ((git-to-us-stream nil)
        (process-object nil))
    (setf git-to-us-stream (make-string-output-stream))
    (unwind-protect
         (progn
           (setf process-object (sb-ext:run-program  "/usr/bin/git" argslist :pty git-to-us-stream :directory directory))
           (values (sb-ext:process-exit-code process-object) (get-output-stream-string git-to-us-stream)))
      (close git-to-us-stream))))

(lefun make-password-response-provider (password)
  (lambda (server-to-us-string us-to-srv-stream)
    "possible return values: :success, :exp-continue, :exp-exit-success, :no-match"
    (format t "server-to-us: ~s~%" server-to-us-string)
    (berryutils:print-transparent
     (cond
      ((not (eql nil (search "Password:" server-to-us-string)))
       (progn
         (format us-to-srv-stream "~a~%" password)
         :success))
      ((not (eql nil (search "yes/no" server-to-us-string)))
       (progn
         (format us-to-srv-stream "yes~%")
         :exp-continue))
      ((> (length server-to-us-string) 8)   ;means we got past the pw prompt
       :exp-exit-success)
      (t
       :no-match)))))

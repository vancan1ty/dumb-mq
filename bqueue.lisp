(require :bordeaux-threads)
(in-package :queues)
;; CB this implementation based on and extended from
;; queues by Eric O'Connor
;; https://github.com/oconnore/queues/blob/master/cqueue.lisp
;; MIT license

(eval-when (:compile-toplevel :load-toplevel :execute)
  (use-package :bordeaux-threads))

(defclass super-cqueue (simple-cqueue)
  ((cv :initform (make-condition-variable)
       :accessor queue-cv)
   (size-bound :initform 1000 :initarg size-bound
               :accessor queue-size-bound)))

(defgeneric qpop-or-wait (q &optional empty)
  (:documentation "pop an item off, or wait to do so till the cv is signalled and then pop your item off!"))

(export 'qpop-or-wait)

(defmethod qpop-or-wait ((q super-cqueue) &optional empty)
  (with-recursive-lock-held ((lock-of q))
    (when (eql (qsize q) 0) ;;then we need to wait for something to be put on the queue!
      (format t "about to wait~%")
      (condition-wait (queue-cv q) (lock-of q)))
    (format t "freed from wait!~%")
    (qpop q empty)))

(defmethod make-queue ((type (eql :super-cqueue)) &key minimum-size copy size-bound)
  (let ((out (make-queue :simple-queue
                         :minimum-size minimum-size
                         :copy copy
                         :class 'super-cqueue)))
    (when size-bound
      (setf (queue-size-bound out) size-bound))
    out))

(defmethod qpush ((q super-cqueue) el)
  (declare (optimize (debug 3) (safety 3) (speed 0)))
  (format t "qpush: qsize: ~a, qsizebound ~a" (qsize q) (queue-size-bound q))
  (with-recursive-lock-held ((lock-of q))
    (when (>= (qsize q) (queue-size-bound q)) ;; CB TODO add proper configurable high-water-mark behavior
      (format t "ERROR: queue ~s filled up!~%" q)
      (return-from qpush))
    (when (eql (qsize q) 0)
      (format t "qpush: about to condition notify~%")
      (condition-notify (queue-cv q)))
    (format t "qpush: going up the chain!~%")
    (call-next-method)))

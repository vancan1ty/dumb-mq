(in-package :berrysync)
;; this is basically a demonstrator plugin for dmq showing
;; basic message handling
;; server sends out a number to client 1, client 1 finds the next prime greater
;; then server sends out that new prime to client 2, client 2 finds the next prime greater
;; and so on and so forth till we reach a set threshold, at which point the server sends
;; shutdown messages to the clients and everybody shuts down.

;;first comes common code

(defparameter *simple-message-format-str*  "~a")

(defparameter +SIMPLE-HANDLER-SERVER-PLUGIN-ID+ "plugin:simple-handler-server")

;;then comes server code
(defclass simple-handler-server (dmq-plugin)
  ;;config
  ((max-value :accessor shs-max-value :initarg max-value :initform 1000)
   (number-of-nodes-to-wait-for :accessor shs-number-of-nodes-to-wait-for
                                :initarg numnodes :initform 2)
   ;;state
   (current-value :accessor shs-current-value :initarg current-value :initform 2)
   (active-endpoints :accessor shs-active-endpoints :initform ())))

(defgeneric find-next-subscriber-endpoint (server endpoint))

(defmethod initialize-instance :after ((this simple-handler-server) &key)
  (setf (plugin-id this) +SIMPLE-HANDLER-SERVER-PLUGIN-ID+))

;;NUR -- number response
(defmethod provide-handled-fncodes ((plugin simple-handler-server))
  (list "NUR"))

(defmethod pre-setup-plugin ((this simple-handler-server) (dmq dmq))
  (call-next-method)) 

(defmethod process-uow ((this simple-handler-server) uow)
  (cond ((find (uow-ifncode uow) *status-update-ifncodes*)
         (v:info :simple-handler-server "discarding status update"))
        ((eql (uow-ifncode uow) :RECEIVED)
         (let ((fncode (message-fncode (uow-attachment uow))))
           (cond
             ((equal fncode "ACK")
              (v:info :simple-handler "received ack for message ~s" (message-id (uow-attachment uow))))
             ((equal fncode "NUR")
              (let ((currvalue (parse-integer (message-body (uow-attachment uow)))))
                (v:info :simple-handler "received number update ~s from ~s" currvalue (uow-originator uow))
                (setf (shs-current-value this) currvalue)
                (if (> currvalue (shs-max-value this)) ;;then we are done! send out SQUs
                    (progn
                      (v:info :simple-handler "sending out squs!")
                      (loop for ep in (shs-active-endpoints this) do
                           (dispatcher-enqueue-uow
                            (plugin-dmq-instance this)
                            (generate-message-send-uow 
                             (make-message "SQU" "")
                             this
                             ep))))
                    (progn ;;else, we will send out 
                      (v:info :simple-handler "sending out SNUs!")
                      (dispatcher-enqueue-uow
                       (plugin-dmq-instance this)
                       (generate-message-send-uow 
                        (make-message "SNU" (write-to-string currvalue))
                        this
                        (find-next-subscriber-endpoint this (uow-originator uow))))))))
             (t
              (v:warn :simple-handler "unsupported fncode! ~s in ~s" fncode uow)))
           )
         )
        (t
         (v:warn :simple-handler "unsupported ifncode ~s" (uow-ifncode uow)))))


(defmethod find-next-subscriber-endpoint ((this simple-handler-server) endpoint)              
  (let ((foundpos -1) out)
    (block loopblock
      (loop for candidate-endpoint in (shs-active-endpoints this)
         for i from 0 do
           (when (equal (endpoint-id candidate-endpoint) (endpoint-id endpoint))
             (setq foundpos i)
             (return-from loopblock))))
    (setq out (elt (shs-active-endpoints this) (mod (+ foundpos 1) (length (shs-active-endpoints this)))))
    (v:info :simple-handler "next-subscriber-endpoint from ~s is ~s" endpoint out)
    out
    ))

(defmethod on-endpoint-connect ((this simple-handler-server) (endpoint endpoint))
  (v:info :simple-handler-server "on-endpoint-connect ~a ~a" this endpoint)
  (push endpoint (shs-active-endpoints this))
  (when (>=  (length (shs-active-endpoints this)) (shs-number-of-nodes-to-wait-for this))
    ;;then we are ready to kick off the messaging!
;;    (break)
    (dispatcher-enqueue-uow
     (plugin-dmq-instance this)
     (generate-message-send-uow 
      (make-message "SNU" (write-to-string (shs-current-value this)))
      this
      endpoint))
    ()))

(defparameter +SIMPLE-HANDLER-CLIENT-PLUGIN-ID+ "plugin:simple-handler-client")

(defclass simple-handler-client (dmq-plugin)
  ((ip-and-port-to-connect-to :accessor shc-ip-and-port-to-connect-to :initarg ip-and-port-to-connect-to)))

(defmethod initialize-instance :after ((this simple-handler-client) &key)
  (setf (plugin-id this) +SIMPLE-HANDLER-CLIENT-PLUGIN-ID+))

;;SNU -- simple number
;;SQU -- simple quit signal
(defmethod provide-handled-fncodes ((o simple-handler-client))
  (list "SNU" "SQU"))

(defmethod process-uow ((this simple-handler-client) uow)
  (cond ((find (uow-ifncode uow) *status-update-ifncodes*)
         (v:info :simple-handler-server "discarding status update"))
        ((eql (uow-ifncode uow) :RECEIVED)
         (let ((fncode (message-fncode (uow-attachment uow))))
           (cond
             ((equal fncode "ACK")
              (v:info :simple-handler-client "ack received ~s" uow))
             ((equal fncode "SNU")
              (let ((currvalue (parse-integer (message-body (uow-attachment uow)))))
                (v:info :simple-handler-client "received prime request ~s from ~s" currvalue (uow-originator uow))
                (dispatcher-enqueue-uow
                 (plugin-dmq-instance this)
                 (generate-message-send-uow 
                  (make-message "NUR" (write-to-string (next-highest-prime currvalue)))
                  this
                  (uow-originator uow)))))
             ((equal fncode "SQU")
              (v:info :simple-handler-client "received sku!  dying")
              (system-exit! 0))
             (t
              (v:error :simple-handler-client "unsupported fncode ~s" fncode)
              ))))
        (t
         (v:warn :simple-handler "unsupported ifncode ~s" (uow-ifncode uow)))))

(defmethod post-setup-plugin ((this simple-handler-client))
  (register-endpoint (plugin-dmq-instance this)
                     (car (shc-ip-and-port-to-connect-to this))
                     (cdr (shc-ip-and-port-to-connect-to this))
                     nil))

(defun next-highest-prime (input) 
  (loop for candidate from (+ input 1)
     for isprime = t do
       (block innerloop
         (loop for i from 2 upto (sqrt candidate) do
              (when (eql (mod candidate i) 0)
                (setq isprime nil)
                (return-from innerloop))))
       (when isprime
         (return-from next-highest-prime candidate))))

(defmethod pre-setup-plugin ((this simple-handler-client) (dmq dmq))
  (call-next-method))

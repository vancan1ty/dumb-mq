(in-package :berrysync)

(defparameter *message-header-length* 22)
(defparameter *message-format-str*  "~1,'0d_~8a_~6,'0d_~3a_")

(defclass message ()
  ((protocol-version :initarg protocol-version :initform 0) ;0-9
   (id :accessor message-id :initarg id :documentation "6 digits") ;6 digits
   (size :accessor message-size :initarg message-size :initform 0 :documentation "size of body in bytes (excludes header)") 
   (fncode :accessor message-fncode :initarg fncode) ; standardized location for three character function code
   (body :accessor message-body :initarg body) ; the message contents, which you can handle however you like
   ))

(defmethod print-object ((object message) stream)
  (format stream "#<b-MESSAGE {~a}>"
          (message-to-string object)))

(defgeneric message-to-string (amessage))

(defun read-string-to-message (str)
  (let ((protocol-version (parse-integer (subseq str 0 1) :radix 16))
        (message-id (subseq str 2 10))
        (message-size (parse-integer (subseq str 11 17)))
        (fncode (subseq str 18 21))
        (body (if (> (length str) 22) (subseq str 22) "")))
    (make-instance 'message 'protocol-version protocol-version 'id message-id 'message-size message-size 'fncode fncode 'body body)))

(defun extract-message-size-from-headerstr (headerstr)
  (parse-integer (subseq headerstr 11 17)))

(defun validate-message-header (str)
  (if (cl-ppcre:scan "^[0-9]_[A-Za-z0-9]{8}_[0-9]{6}_[A-Za-z0-9]{3}_$" str)
      t
      nil))

(defmethod message-to-string ((message message))
  (concatenate 'string
               (format nil *message-format-str*
                       (slot-value message 'protocol-version)
                       (slot-value message 'id)
                       (slot-value message 'size)
                       (slot-value message 'fncode))
               (if (and (slot-value message 'body) (> (length (slot-value message 'body)) 0))
                    (message-body message))))

(defun make-message (fncode body)
  (let ((outmessage (make-instance 'message 'fncode fncode 'body body 'id (random-alphanumeric-string 8))))
    (setf (slot-value outmessage 'size) (calculate-message-size outmessage))
    outmessage))

(defun calculate-message-size (message)
  (if (eql nil (message-body message))
      0
      (length (message-body message))))


;;(length (message-format (make-instance 'message 'sequence-number 123 'total-message-size 0 'fncode "CHN")))
;;(closer-mop:class-slots message)
;;(mapcar #'closer-mop:slot-definition-name (closer-mop:class-slots (class-of (make-instance 'message))))

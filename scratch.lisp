
(in-package :berrysync)
(format (usocket:socket-stream (slot-value (dmq-state *my-dmq*) 'socket)) (message-to-string message))
(force-output (usocket:socket-stream (slot-value (uow-destination uow) 'socket)))

(format (usocket:socket-stream (elt (state-open-sockets-list (dmq-state *my-dmq*)) 1)) "hello")
(force-output  (usocket:socket-stream (elt (state-open-sockets-list (dmq-state *my-dmq*)) 1)))

(format (usocket:socket-stream (elt (state-open-sockets-list (dmq-state *my-dmq*)) 2)) "hello")
(force-output  (usocket:socket-stream (elt (state-open-sockets-list (dmq-state *my-dmq*)) 2)))

(defparameter *tsocket* (elt (state-open-sockets-list (dmq-state *my-dmq*)) 1))

(sb-bsd-sockets:non-blocking-mode (slot-value *tsocket* 'usocket::socket))
;; I can set non-blocking mode in SBCL using the above


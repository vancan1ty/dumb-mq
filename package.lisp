(defpackage #:berrysync
  (:use #:cl #:alexandria #:split-sequence #:berryutils)
  (:documentation "someday will be useful for file synchronization.")
  (:export
   ;; message core
   #:message
   #:read-string-to-message
   #:message-to-string
   ;; dumb-expect
   #:dumb-expect))

(defpackage #:berrysync-user
  (:use #:cl #:alexandria #:berrysync #:split-sequence))

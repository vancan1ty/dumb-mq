;; CB TODO bound queues
;; CB add ability to stop and start dispatcher/socketer/other execution units
(in-package :berrysync)

(declaim (optimize (speed 0) (safety 3) (space 0) (debug 3)))

(defconstant +PROTOCOL-VERSION+ 0)

(defparameter *status-update-ifncodes* (list :USEND :SSEND :NOTACCEPTED :FAILURE :SUCCESS))

(defparameter *all-ifncodes 
  (list :SEND :RECEIVED :USEND :SSEND :NOTACCEPTED :FAILURE :SUCCESS))

;;; the dmq class pretty much defines shared state which dmq uses within the socketer, dispatcher, and worker threads
(defclass dmq ()
  ((app-running :accessor dmq-app-running :initform nil)
   (state :accessor dmq-state :initarg state :initform nil)
   (config :accessor dmq-config :initarg config :initform nil)
   (queue :accessor dmq-queue :initform (queues:make-queue :super-cqueue))
   (socket-sender-queue :accessor dmq-socket-sender-queue :initform (queues:make-queue :super-cqueue))
   (threadpool :accessor dmq-threadpool :initform (cl-threadpool:make-threadpool 1))
   (socketer-thread :accessor dmq-socketer-thread :initform nil)
   (dispatcher-thread :accessor dmq-dispatcher-thread :initform nil)
   (socket-sender-thread :accessor dmq-socket-sender-thread :initform nil)
   )
  (:documentation "the main class for dumb-mq"))

(defgeneric setup-dmq (config)
  (:documentation "creates a fresh dmq and sets it up"))
(defgeneric start-dmq (dmq loop-forever)
  (:documentation "actually starts the various threads"))
(defgeneric run (dmq)
  (:documentation "starts everything up"))
(defgeneric run-socketer (dmq)
  (:documentation "starts the socketer runloop"))
(defgeneric run-dispatcher (dmq)
  (:documentation "starts the dispatcher runloop"))
(defgeneric dmq-quit (dmq fullquit)
  (:documentation "closes all connections and quits the runloop"))
(defgeneric register-plugin (dmq plugin)
  (:documentation "integrates a plugin into dmq"))
(defgeneric unregister-plugin (dmq plugin)
  (:documentation "hopefully removes all the traces of the plugin from the dmq instance"))
(defgeneric register-endpoint (dmq ip port alreadyconnected)
  (:documentation "connects to the given ip/port if not alreadyconnected, adds it the resulting endpoint to tracking."))
(defgeneric destroy-endpoint (dmq endpoint)
  (:documentation "disconnects from the given endpoint and destroys vestiges of it"))
(defgeneric dispatcher-enqueue-uow (dmq uow)
  (:documentation "enqueues a uow to be processed by the dispatcher"))
(defgeneric sockets-send-uow-literal (dmq uow)
  (:documentation "sends the given message to the given endpoint (call by plugins)"))
(defgeneric socketer-process-socket-wake-up (dmq socket))
(defgeneric socketer-process-socket-wake-up-inline (dmq socket))
;;(defgeneric socketer-read-rest-of-message-from-socket (dmq headerstring socket))
(defgeneric dispatch-status-update-uow (dmq uow))
(defgeneric dispatch-received-uow (dmq uow))
(defgeneric dispatch-handle-built-in-message-uow (dmq uow))
(defgeneric dispatcher-enqueue-keepalive (dmq endpoint))
(defgeneric register-socket-as-endpoint (dmq socket))
(defgeneric lookup-entity-by-id (dmq entity-id))
(defgeneric dispatch-send-uow (dmq uow))

(defmethod setup-dmq (config)
  (declare (type dmq-config config))
  (let ((out  (make-instance 'dmq 'state (make-instance 'dmq-state) 'config config)))
    (when (slot-value config 'listen-port)
      (setf (slot-value (dmq-state out) 'listen-socket)
            (usocket:wrap-in-berry-stream-server-usocket (usocket:socket-listen usocket:*wildcard-host* (slot-value config 'listen-port) :reuse-address t)))
      (setf (slot-value (slot-value out 'state) 'open-sockets-list)
            (append
             (list (slot-value (slot-value out 'state) 'listen-socket))
             (slot-value (slot-value out 'state) 'open-sockets-list))) ;;add the server socket to the listened on
      )
    (loop for plugin in (slot-value config 'plugins) do
         (register-plugin out plugin))
    out))

(defmethod start-dmq ((dmq dmq) loop-forever)
  (cl-threadpool:start (dmq-threadpool dmq))
  (setf (dmq-socketer-thread dmq)
        (bordeaux-threads:make-thread (lambda () (run-socketer dmq)) :name "socketer"))
  (setf (dmq-dispatcher-thread dmq)
        (bordeaux-threads:make-thread (lambda () (run-dispatcher dmq)) :name "dispatcher"))
  (setf (dmq-socket-sender-thread dmq)
        (bordeaux-threads:make-thread (lambda () (run-socket-sender dmq)) :name "socket-sender"))
  (setf (dmq-app-running dmq) t)
  (when loop-forever
    (loop while (dmq-app-running dmq) do
         (sleep 120)))
  dmq)

;;;; the next portion of the code contains the socketer runloop and helper methods
;;;; =============================================================
;;(write-byte byte stream)

(defmethod run-socketer ((dmq dmq))
  "waits for changes on open-sockets-list, and periodically tests sockets to make sure they are still open.
   after calls to wait-for-input, loops through last-read-time-map, and sends out keepalives if necessary
   also accepts new connection requests"
  (declare (optimize (debug 3)))
  (v:info :socketer "starting socketer")
  (unwind-protect
       (progn
         (do () (())
           (handler-bind 
               ((error #'(lambda (data)
                           (v:error :socketer "catching error ~a" data)
                           (invoke-debugger data)
                           )))
               (progn
                 (v:info :socketer "socketer loop iteration")
                 (let (readable-sockets)
                   (setf readable-sockets
                         (usocket:wait-for-input (slot-value (dmq-state dmq) 'open-sockets-list)
                                                 :ready-only t :timeout (config-wait-timeout (dmq-config dmq))))
                   (v:info :socketer "wait-for-input woke up on ~a!" readable-sockets)
                   ;;processing woke sockets
                   (when (not (eql readable-sockets nil)) ;if not nil then we received some data!
                     (loop for socket in readable-sockets do
                              (socketer-process-socket-wake-up dmq socket)))
                   ;;now, taking care of keepalives which we need to send
                   (maphash
                    (lambda (endpoint-id endpoint)
                      (declare (ignore endpoint-id))
                      (let ((time-difference-ms (- (get-internal-real-time) (slot-value endpoint 'last-read-time))))
                        (cond
                          ((> time-difference-ms
                              (* 1000 (config-keepalive-deletion-threshold (dmq-config dmq))))
                              (v:warn :socketer "destroying endpoint ~a due to last read time exceeding threshold" endpoint)
                              (destroy-endpoint dmq endpoint)
                              )
                          ((> time-difference-ms
                              (* 1000 (slot-value (dmq-config dmq) 'keepalive-transmission-threshold)))
                           (dispatcher-enqueue-keepalive dmq endpoint))
                          (t nil) ; do nothing
                          ))
                      )
                    (state-endpoint-id-to-endpoint (dmq-state dmq))
                    ))))))
    (progn
      (v:warn :runloop "exiting dmq")
      (dmq-quit dmq nil)
    )))

(defmethod run-socket-sender ((dmq dmq))
  "waits for input on the socket-sender work queue.  sends the messages as it finds them (nonblocking sends and saving partial work)"
  (declare (optimize (debug 3)))
  (v:info :socket-sender "starting socket-sender")
  (do () (())
    (handler-bind
        ((error #'(lambda (data)
                    (v:error :socket-sender "an error occured: ~a" data)
                    (invoke-debugger data))))
      (let ((uow (queues:qpop-or-wait (dmq-socket-sender-queue dmq))))
        (v:info :socket-sender "socket-sender processing ~s" uow)
        (sockets-send-uow-literal dmq uow)
        ))))


;; CB MEBBEDO make this call into
;; create-worker-task/schedule-threadpool-job logic instead of
;; doing it directly
(defmethod socketer-process-socket-wake-up ((dmq dmq) socket)
  (handler-bind
      ((error #'(lambda (data)
                  (v:error :socketer "~a while reading from socket ~a" data socket)
                  (when (typep socket 'usocket:stream-usocket)
                    (destroy-endpoint dmq (gethash (usocket:get-socket-id socket) (state-endpoint-id-to-endpoint (dmq-state dmq))))
                    )
                  (invoke-debugger data)
                  )))
    (socketer-process-socket-wake-up-inline dmq socket)))

(defmethod socketer-process-socket-wake-up-inline ((dmq dmq) socket)
  ;;  (break)
  (v:info :socketer "process-socket-wake-up-inline ~a" socket)
  (cond ;;switch on socket type
    ((typep socket 'usocket:stream-server-usocket) ;;then make a new socket and add it to our list!
     (let ((nsocket (usocket:wrap-in-berry-stream-usocket (usocket:socket-accept socket))))
       (v:info :socketer "new connection detected from ~a" nsocket)
       (register-socket-as-endpoint dmq nsocket)
       (v:info :socketer "sucessfully registered ~a" nsocket)
       ))
    ((typep socket 'usocket:stream-usocket)
     ;; read the input from the socket in a nonblocking manner, save the work
     ;; in progress to the in-pmo slot of the endpoint class
     (v:info :socketer "typep stream usocket")
     (let* ((endpoint
             (gethash (usocket:get-socket-id socket) (state-endpoint-id-to-endpoint (dmq-state dmq))))
            (pmo (endpoint-pmo-in endpoint))
            mmessage)
       (when (eql pmo nil)
         (setf pmo (make-instance 'pmo-in)))
       (when (pmo-in-is-in-header-mode pmo)
         (let (remaining-in-header current-header-filled-length)
           (setq remaining-in-header (- *message-header-length* (pmo-header-filled-length pmo)))
           (setq current-header-filled-length (pmo-header-filled-length pmo))
           (assert (> remaining-in-header 0))
           (v:info :socketer "about to read from ~a in header mode with ~a remaining" (usocket:socket-stream socket) remaining-in-header)
           (setf (pmo-header-filled-length pmo)
                 (read-sequence
                  (pmo-header pmo)
                  (usocket:socket-stream socket) :start (pmo-header-filled-length pmo)))
           (v:info :socketer "header after read ~a" (pmo-header pmo))
           (when (eql (pmo-header-filled-length pmo) current-header-filled-length)  ;;then I believe this means that the connection is closed
             (v:info :socketer "0-length body read. Connection shut down by remote ~a~%" socket)
             (destroy-endpoint dmq endpoint)
             (return-from socketer-process-socket-wake-up-inline nil))
           (setf (endpoint-last-read-time endpoint) (get-internal-real-time))
           (when (< (pmo-header-filled-length pmo) *message-header-length*)
             ;;then we couldn't fill up the header.  try again later
             (v:info :socketer "couldn't fill up header. partial contents ~s" (pmo-header pmo))
             (return-from socketer-process-socket-wake-up-inline nil))))
       (when (not (validate-message-header (pmo-header pmo)))
         (v:info :socketer "invalid header!.  drop message, and in future, possibly message other end to inform")
         (break)
         (setf (endpoint-pmo-in endpoint) nil)
         (return-from socketer-process-socket-wake-up-inline nil))
       ;;IFF we got here, then the header has been filled up and validated
       (setf (pmo-eventual-body-length pmo) (extract-message-size-from-headerstr (pmo-header pmo)))
       (setf (pmo-body pmo) "")
       (when (> (pmo-eventual-body-length pmo) 0)
         (setf (pmo-body pmo) (make-string (pmo-eventual-body-length pmo)))
         (let ((orig-filled-length (pmo-body-filled-length pmo))
               (num-bytes-read 0))
           (v:info :socketer "about to read ~a bytes from ~a in body read mode" (- (pmo-eventual-body-length pmo) orig-filled-length) (usocket:socket-stream socket))
           ;;         (break)
           ;; replacing read-sequence-nonblock with read-sequence for now
           ;; as read-sequence-nonblock has at least one bug (seems to be consuming one more character than it should sometimes)
           (setf (pmo-body-filled-length pmo) (read-sequence (pmo-body pmo) (usocket:socket-stream socket) :start orig-filled-length))
           (v:info :socketer "body after read ~a" (pmo-body pmo))
           (setf (endpoint-last-read-time endpoint) (get-internal-real-time))
           (setq num-bytes-read (- (pmo-body-filled-length pmo) orig-filled-length))
           (v:info :socketer "read ~a characters of body from ~a. [~a/~a]  \"~s\"~%"
                   num-bytes-read socket (pmo-body-filled-length pmo) (pmo-eventual-body-length pmo)
                   (pmo-body pmo))
           (when (eql num-bytes-read 0) ;;then I believe this means that the connection is closed
             (v:info :socketer "0-length body read. Connection shut down by remote ~a~%" socket)
             (destroy-endpoint dmq endpoint)
             (return-from socketer-process-socket-wake-up-inline nil))
           (when (< (pmo-body-filled-length pmo) (pmo-eventual-body-length pmo))
             ;;then not the whole body was filled.  let select fill it in later
             (return-from socketer-process-socket-wake-up-inline nil)
             )))
       ;;if we got here, then we should have read the whole message.  we can process that now
       (setq mmessage (pmo-in-convert-to-message pmo))
       ;;if we have gotten to this point, then we should have a message to work with!
       ;;put it on the queue
       (queues:qpush (dmq-queue dmq) (generate-message-receipt-uow mmessage endpoint))
       ;;clear the incoming pmo for this endpoint
       (setf (endpoint-pmo-in endpoint) nil)
       ;;send an ack right now
       (when (not (equal (message-fncode mmessage) "ACK"))
         (dispatch-send-uow dmq (generate-message-send-uow (make-message "ACK" (message-id mmessage)) :socketer endpoint)))))
    (t
     (format t "ERROR: unsupported socket type for argument ~a~%" socket))
    ))

(defun pmo-in-convert-to-message (pmo)
  (read-string-to-message (concatenate 'string (pmo-header pmo) (pmo-body pmo))))

;; (defmethod socketer-read-rest-of-message-from-socket ((dmq dmq) headercandidate socket)
;;   (handler-bind
;;       ((error #'(lambda (data)
;;                   (v:error :socketer "error ~a reading rest of ~a from ~a" data headercandidate socket)
;;                   (invoke-debugger data)
;;                   nil)))
;;   (let (body-length (bodystring nil))
;;         (progn
;;           (setq body-length (parse-integer (subseq headercandidate 9 15)))
;;           (when (> body-length 0)
;;             (read (usocket:socket-stream socket))
;;             (setq bodystring (read-sequence (make-string body-length) (usocket:socket-stream socket))))
;;           (read-string-to-message (concatenate 'string headercandidate "_" bodystring))))
;;     ))

(defmethod sockets-send-uow-literal ((dmq dmq) uow)
  "sends the message attachment of the given uow to the uow's endpoint.  generally returns a uow wrapping
the original UOW with one of :SSEND or :USEND as the status code"
  (handler-bind
      ((error #'(lambda (data) 
                  (v:log :sockets-sender "error sending ~a" uow)
                  (generate-usend-uow :sockets-sender uow data)
                  (invoke-debugger data))))
    (let ((message (uow-attachment uow)))
      (v:info :send "sending ~s to ~s" message (endpoint-id (uow-destination uow))) 
      (format (usocket:socket-stream (slot-value (uow-destination uow) 'socket)) (print-transparent-m "myo" (message-to-string message)))
      (force-output (usocket:socket-stream (slot-value (uow-destination uow) 'socket)))
      (generate-ssend-uow :sockets-sender uow)))
  )


;;;; the next portion of the code contains the dispatcher runloop and helper methods
;;;; =============================================================

(defparameter +DMQ-BUILT-IN-FNCODES+ (list "KAL" "ACK"))

(defmethod run-dispatcher ((dmq dmq))
  "waits for input on the dmq work queue.  farms it out to the thread pool or plugins as needs be"
  (declare (optimize (debug 3)))
  (progn
    (do () (())
      (handler-bind
          ((error #'(lambda (data)
                      (v:error :dispatcher "an error occured: ~a" data)
                      (invoke-debugger data))))
        (v:info :dispatcher "starting dispatcher")
        (let ((uow (queues:qpop-or-wait (dmq-queue dmq))))
          (v:info :dispatcher "dispatcher processing ~s" uow)
          (cond 
            ((eql (uow-ifncode uow) :SEND) ;;then this is something that needs to be sent out
             (dispatch-send-uow dmq uow))
            ((eql (uow-ifncode uow) :RECEIVED) ;;includes the case of acks
             (dispatch-received-uow dmq uow))
            ((find (uow-ifncode uow) *status-update-ifncodes*) ;;SSEND, USEND, NOTACCEPTED and the like
             (dispatch-status-update-uow dmq uow))
            (t (v:warn :dispatcher "unsupported ifncode for uow ~s" uow)))))
      )))

(defmethod dispatch-send-uow ((dmq dmq) uow)
  "put the uow on the socket sender queue for outbound processing"
  (push uow (endpoint-outqueue (uow-destination uow))) 
  (queues:qpush (dmq-socket-sender-queue dmq) uow))
;  (push uow (dmq-socket-sender-queue dmq)))

(defmethod dispatch-status-update-uow ((dmq dmq) uow)
  (let* ((original-uow (car (uow-attachment uow)))
         (originator (uow-originator original-uow)))
    (v:info :dispatcher "handling status-update-uow ~s" uow)
    (if (typep originator 'dmq-plugin)
        (process-uow originator uow)
        (v:info :dispatcher "discarding status update uow"))))

(defmethod dispatch-received-uow ((dmq dmq) uow)
  (let ((mmessage (uow-attachment uow)))
    (cond
      ((position (slot-value mmessage 'fncode) +DMQ-BUILT-IN-FNCODES+ :test 'equal)
       (dispatch-handle-built-in-message-uow dmq uow))
      (t
       (let ((plugin-to-use (gethash (slot-value mmessage 'fncode) (state-fncode-to-plugin-table (dmq-state dmq)))))
         (when (not plugin-to-use)
           (v:error "failed to find plugin for fncode ~s" (slot-value mmessage 'fncode))
           (return-from dispatch-received-uow nil))
         (if (typep plugin-to-use 'dmq-plugin)
             (process-uow plugin-to-use uow)
             (v:info :dispatcher "did not find plugin to use to handle ~a" uow)))))))

(defmethod dispatch-handle-built-in-message-uow ((dmq dmq) uow)
  (let ((fncode (message-fncode (uow-attachment uow))))
    (cond ;;switch on command from client
      ((equal fncode "KAL") ;;keepalive
       (v:error :dispatcher "keepalives should be handled at the socketer layer now!"))
      ((equal fncode "ACK") ;;message acknowledgement
       (dispatcher-handle-incoming-ack dmq uow))
      (t
       (break)
       (v:error :dispatcher "we should never get here.  fncode ~s!!" fncode)))))



(defun dispatcher-handle-incoming-ack (dmq uow)
  (declare (ignore dmq))
  ;; we just use origuow as a means of finding who originally sent the message
  (let (origuow (queuepos (find-message-id-in-ep-outqueue (uow-originator uow) (message-body (uow-attachment uow)))))
    (when (not (>= queuepos 0)) ;;then we were not waiting on this message ack! signal an error and stop
      (break)
      (v:error :dispatcher "unexpected ack ~a" uow)
      (return-from dispatcher-handle-incoming-ack nil))
    (setq origuow (elt (endpoint-outqueue (uow-originator uow)) queuepos)) 
    (setf (endpoint-outqueue (uow-originator uow)) (delete-if (constantly t) (endpoint-outqueue (uow-originator uow)) :start queuepos :end (1+ queuepos)))
    (when (typep (uow-originator origuow) 'dmq-plugin)
      (process-uow (uow-originator origuow) uow))
    ))

(defmethod dispatcher-enqueue-keepalive ((dmq dmq) endpoint)
  (let ((uow-to-send (generate-message-send-uow (make-message "KAL" "") :socketer endpoint)))
    (v:info :socketer "socketer enqueing keepalive ~a ~%" uow-to-send)
    (dispatcher-enqueue-uow dmq uow-to-send)))

(defmethod dispatcher-enqueue-uow ((dmq dmq) uow)
  (v:info :enqueue "enqueing ~s" uow) 
  (queues:qpush (dmq-queue dmq) uow))

(defmethod lookup-entity-by-id ((dmq dmq) entity-id)
  "eventually should support looking up any entity in the program by its alphanumeric id.  for now, just looks up plugins and builtins"
  (cond
    ((find entity-id (list :socketer :dispatcher :threadtask))
     entity-id)
    ((eql 0 (search "plugin:" entity-id))
     (loop for plugin in (slot-value (dmq-state dmq) 'active-plugins-list) do
          (if (equal (plugin-id plugin) entity-id)

              (return-from lookup-entity-by-id plugin)))
     nil)
    (t
     (v:error :lookup "unsupported entity at lookup: ~a" entity-id)
     )))

(defun find-message-id-in-ep-outqueue (endpoint message-id)
  (loop for uow in (endpoint-outqueue endpoint)
     for i from 0 do
       (let ((message (uow-attachment uow)))
         (if (print-transparent-m "yoyoyo"  (equal message-id (message-id message)))
             (return-from find-message-id-in-ep-outqueue i))))
  -1)
       


;;;; the next portion of the code contains general dmq methods
;;;; =============================================================

(defmethod dmq-quit ((dmq dmq) fullquit)
  (setf (dmq-app-running dmq) nil)
  (loop for endpoint in
       (loop for endpoint being the hash-values of
            (slot-value (dmq-state dmq) 'endpoint-id-to-endpoint) collect endpoint) do
       (destroy-endpoint dmq endpoint))
  (when (slot-value (dmq-state dmq) 'listen-socket)
    (usocket:socket-close (slot-value (dmq-state dmq) 'listen-socket)))
  (cl-threadpool:stop (dmq-threadpool dmq))
  (if (not (eql (dmq-socketer-thread dmq) (bt:current-thread)))
      (bt:destroy-thread (dmq-socketer-thread dmq)))
  (if (not (eql (dmq-socket-sender-thread dmq) (bt:current-thread)))
      (bt:destroy-thread (dmq-socket-sender-thread dmq)))
  (if (not (eql (dmq-dispatcher-thread dmq) (bt:current-thread)))
      (bt:destroy-thread (dmq-dispatcher-thread dmq)))
  (if fullquit
      (system-exit! 0)))

(defmethod register-plugin ((dmq dmq) (plugin dmq-plugin))
  (v:info :register-plugin "register-plugin ~s" plugin)
  (pre-setup-plugin plugin dmq)
  (setf (slot-value (dmq-state dmq) 'active-plugins-list) (append (slot-value (dmq-state dmq) 'active-plugins-list) (list plugin)))
  (loop for fncode in (provide-handled-fncodes plugin) do
       (setf (gethash fncode (slot-value (dmq-state dmq) 'fncode-to-plugin-table)) plugin))
  (post-setup-plugin plugin)
  )

(defmethod unregister-plugin ((dmq dmq) (plugin dmq-plugin))
  (destroy-plugin plugin)
  (setf (slot-value (dmq-state dmq) 'active-plugins-list) (remove plugin (slot-value (dmq-state dmq) 'active-plugins-list)))
  (loop for fncode in (provide-handled-fncodes plugin) do
       (remhash fncode (slot-value (dmq-state dmq) 'fncode-to-plugin-table))))

(defmethod register-endpoint ((dmq dmq) ip port alreadyconnected)
  (v:info :register-endpoint "register-endpoint ~s ~s ~s" ip port alreadyconnected)
  (let (msocket)
    (if alreadyconnected
        (progn
          (setq msocket (gethash (cons ip port) (slot-value (dmq-state dmq) 'ip-and-port-to-socket)))
          (when (not msocket)
            (signal (make-condition 'simple-error :name (format t "socket to ~s, ~s not already connected as indicated!" ip port)))))
        (progn
          (setq msocket (usocket:wrap-in-berry-stream-usocket (usocket:socket-connect ip port)))
          (assert (not (eql msocket nil)))
          (register-socket-as-endpoint dmq msocket)))))

(defmethod register-socket-as-endpoint ((dmq dmq) socket)
  (v:info :register-socket-as-endpoint "register-socket-as-endpoint ~a" socket)
  (setf (state-open-sockets-list (dmq-state dmq))
        (append (print-transparent-m "osocklist" (state-open-sockets-list (dmq-state dmq))) (list socket)))
  (v:info :register-socket-as-endpoint "noyo1")
  (setf (gethash (usocket:get-socket-id socket) (state-endpoint-id-to-endpoint (dmq-state dmq)))
        (make-instance 'endpoint 'socket socket
                       'last-read-time (get-internal-real-time)))
  (v:info :register-socket-as-endpoint "noyo2")
  (loop for plugin in (slot-value (dmq-state dmq) 'active-plugins-list) do
       (v:info :register-socket-as-endpoint "noyo3 ~a" plugin)
       (on-endpoint-connect plugin (gethash (usocket:get-socket-id socket) (slot-value (dmq-state dmq) 'endpoint-id-to-endpoint))))
  (v:info :register-socket-as-endpoint "noyo4")
  )

(defmethod destroy-endpoint ((dmq dmq) endpoint)
  (when (not (typep endpoint 'endpoint))
    (v:error :destroy-endpoint "nothing to destroy here!"))
  (v:info :destroy-endpoint "destroy-endpoint ~a" endpoint)
  (assert (typep (slot-value endpoint 'socket) 'usocket:stream-usocket))
  (remhash (endpoint-id endpoint) (state-endpoint-id-to-endpoint (dmq-state dmq)))
  (setf (state-open-sockets-list (dmq-state dmq))
        (delete (endpoint-socket endpoint)
                (state-open-sockets-list (dmq-state dmq))))
  (v:info :destroy-endpoint "here1")
  (loop for plugin in (slot-value (dmq-config dmq) 'plugins) do
       (on-endpoint-shutdown plugin endpoint))
  )

(defclass dmq-state ()
  ((listen-socket :accessor state-listen-socket :initarg listen-socket :initform nil :documentation "nil if this instance is not configured to accept connections")
   (open-sockets-list :accessor state-open-sockets-list :initarg open-sockets-list :initform ())
   (pending-read-sockets-list :accessor state-pending-read-sockets-list :initform ())
   (pending-write-sockets-list :accessor state-pending-write-sockets-list :initform ())
   (endpoint-id-to-endpoint :accessor state-endpoint-id-to-endpoint :initarg endpoint-id-to-endpoint :initform (make-hash-table :test 'equal)
                   :documentation "a map of endpoint ids (of form 10.0.2.45:12451) to endpoint-data objects")
   (active-plugins-list :accessor state-active-plugins-list :initarg active-plugins-list :initform ()) ;;CB MEBBEDO refactor this into hashmap for better performance
   (fncode-to-plugin-table :accessor state-fncode-to-plugin-table :initarg fncode-to-plugin-table :initform (make-hash-table :test 'equal)
                           :documentation "maps fncodes to their handler plugins")
   ))

;;(defun remove-socket-from-pending-read-list (dmq-state)
;;  (bordeaux-threads:with-recursive-lock-held 

(defclass dmq-config ()
  ((listen-port
    :accessor config-listen-port
    :initarg listen-port
    :initform nil
    :documentation "port to listen on if this instance should be configured to accept connections, nil otherwise")
   (wait-timeout
    :accessor config-wait-timeout
    :initarg wait-timeout
    :initform (* 60 1)
    :documentation "controls the amount of time we listen on the socket till we fall back to our keepalive checks and such")
   (keepalive-transmission-threshold
    :accessor config-keepalive-transmission-threshold
    :initarg keepalive-transmission-threshold
    :initform (* 60 3)
    :documentation "if we haven't heard from the other end in X minutes, send it a keepalive!")
   (keepalive-deletion-threshold
    :accessor config-keepalive-deletion-threshold
    :initarg keepalive-deletion-threshold
    :initform (* 60 7)
    :documentation "v if we haven't heard from the other end in over X minutes, delete it (server) or close connection and try to reconnect (client)!")
   (plugins
    :accessor config-plugins
    :initarg plugins
    :initform ()
    :documentation "a list of make-instanced plugins which dmq will call pre-/post setup-plugin for later on")
   )
  (:documentation "construct with make-instance"))

(defclass pmo-out ()
  ((total-string-to-send :accessor pmo-total-string-to-send :initarg total-string-to-send :initform "")
   (remaining-string-to-send :accessor pmo-remaining-string-to-send :initform ""))
  (:documentation "holds the contents of an in-progress message send")
  )

(defmethod initialize-instance :after ((this pmo-out) &key)
  (setf (pmo-remaining-string-to-send this) (pmo-total-string-to-send this)))

(defclass pmo-in ()
  (
   (header :accessor pmo-header :initform (make-string *message-header-length*))
   (header-filled-length :accessor pmo-header-filled-length :initform 0)
   (body :accessor pmo-body :initform "")
   (body-filled-length :accessor pmo-body-filled-length :initform 0)
   (eventual-body-length :accessor pmo-eventual-body-length :initform nil))
  (:documentation "holds the contents of an in-progress message read")
  )

(defun pmo-in-is-in-header-mode (pmo)
  "if returns t, then we are still trying to fill the header of the current message 
on the endpoint.  
   if returns nil, then we have already read the header and it is stored in the in-pmo body"
  (if (< (pmo-header-filled-length pmo) *message-header-length*)
      t
      nil))

(defclass endpoint ()
  ((endpoint-id :accessor endpoint-id :initform nil)
   (socket :accessor endpoint-socket :initarg socket)
   (pmo-in :accessor endpoint-pmo-in :initform nil
           :documentation "holds the current in-progress message being read on the endpoint")
   (pmo-out :accessor endpoint-pmo-out :initform nil
            :documentation "holds in progresss message which we are working on sending.  when there is nothing in progress, should be nil.")
   (outqueue :accessor endpoint-outqueue :initarg outqueue :initform ()
             :documentation "queue of outbound uows.  these are things which have been 
enqueued by the dispatcher, but may not have actually been sent yet.  kept in list 
until we receive the ack.")
   (last-read-time :accessor endpoint-last-read-time :initarg last-read-time :initform 0
                   :documentation "last time we read any form of packet from this destination (unix time)")))

(defmethod initialize-instance :after ((endpoint endpoint) &key)
  (setf (endpoint-id endpoint) (usocket:get-socket-id (endpoint-socket endpoint)))
  (v:info :initialize-instance "initialized endpoint-id to ~a" (endpoint-id endpoint)))

(defmethod print-object ((object endpoint) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "endpoint-id: ~a, socket: ~a, pmo-in: ~a, pmo-out: ~a, outqueue_size: ~a, last-read-time: ~a"
            (endpoint-id object) (endpoint-socket object) (endpoint-pmo-in object)
            (endpoint-pmo-out object) (length (endpoint-outqueue object))
            (endpoint-last-read-time object))))


;(mod most-positive-fixnum 999999)

;;callback immediately after sending ifncodes
;; SSEND -- successful send
;; USEND -- unsuccesful send
(defclass uow ()
  ((id :accessor uow-id :initform nil :documentation "random long number.  used to uniquely identify a given request lifecycle")
   (ifncode :accessor uow-ifncode :initform nil :initarg ifncode
            :documentation "internal fncode, which should be a keyword arg like :SEND or :RECEIVED or :SUCCESS 
r :FAILURE.  If a given entity cannot handle a fncode, it should signal an exception and reject the message 
(send back a :NOTACCEPTED error")
   (originator :accessor uow-originator :initform nil :initarg originator :documentation "after successful or failure completion, will make a new wqueue entry wrapping the data and will call this function")
   (destination :accessor uow-destination :initform nil :initarg destination)
   (attachment :accessor uow-attachment :initform nil :initarg attachment)))

(defmethod print-object ((object uow) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "id: ~a, ifncode: ~a, originator: ~a, destination: ~a, attachment: ~a"
            (uow-id object) (uow-ifncode object) (uow-originator object)
            (uow-destination object) (uow-attachment object))))

;; (defmethod print-object ((object uow) stream)
;;   (print-unreadable-object (object stream :type t :identity t)
;;     (format stream "{ ~s ~s}" (type-of object)
;;             (loop for i in (mapcar #'closer-mop:slot-definition-name (closer-mop:class-slots (class-of object)))
;;              collect (cons i (slot-value object i))))))

(defmethod initialize-instance :after ((uow uow) &key)
  (setf (uow-id uow) (random most-positive-fixnum)))

(defgeneric submit-uow-to-dest (uow dest)
  (:documentation "this method should be responsible for submitting the uow to dest"))

;; (defclass message-uow (uow)
;;   ((message :initform nil)))
;;    (message :initform nil :documentation "the underlying network message repr (see message class)")

(defun generate-message-send-uow (message originator destination)
  (make-instance 'uow 'ifncode :SEND 'originator originator 'destination destination 'attachment message))

(defun generate-message-receipt-uow (mmessage endpoint)
  (make-instance 'uow 'ifncode :RECEIVED 'originator endpoint 'destination :dispatcher 'attachment mmessage))

(defun generate-ssend-uow (generator original-uow)
  (make-instance 'uow 'ifncode :SSEND 'originator generator 'destination :dispatcher 'attachment (list original-uow nil)))

(defun generate-usend-uow (generator original-uow error-data)
  (make-instance 'uow 'ifncode :USEND 'originator generator 'destination :dispatcher 'attachment (list original-uow error-data)))

(defun generate-task-failure-uow (generator original-uow error-data)
  (make-instance 'uow 'ifncode :FAILURE 'originator generator 'destination :dispatcher 'attachment (list original-uow error-data)))

(defun create-and-schedule-threadpool-job (dmq uow funtodo)
  (cl-threadpool:add-job (dmq-threadpool dmq)
                         (create-worker-task dmq uow funtodo)))
      
(defun create-worker-task (dmq uow funtodo)
  "funtodo shoul return a UOW, typically a SSEND or USEND uow"
  (lambda ()
    (handler-bind
        ((error #'(lambda (data)
                    (format t "ERROR:h ~a~%" data)
                    (queues:qpush (dmq-queue dmq) (generate-task-failure-uow :threadtask uow data))
                    (invoke-debugger data)
                    )))
      (let ((result (funcall funtodo dmq uow)))
        (queues:qpush (dmq-queue dmq) result))
      )))




;; valid originators
;; endpoint-id (along lines of 10.200.53.11:9090 )
;; plugin-id (unique string, prefixed by plugin.  example plugin:berrysync)
;; :dispatcher
;; :socketer
;; :threadtask

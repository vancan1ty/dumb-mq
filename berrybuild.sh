#!/bin/sh 
#requires your lisp to be setup with quicklisp/asdf

preconfig() {
	 echo "nothing todo"
}

configure() {
	 echo "in the future, check for lisp packages here"
	# ./configure --prefix=/berrysystem
}

build() {
    sbcl --disable-debugger --quit --eval "(progn  (push '*default-pathname-defaults* asdf:*central-registry*) (asdf:operate 'asdf:load-op :berrysync))"
}

check() {
    sbcl --disable-debugger --quit --eval "(progn  (push '*default-pathname-defaults* asdf:*central-registry*) (asdf:operate 'asdf:test-op :berrysync))"
}

install() {
	 echo "nothing todo"
	#make install
}

# run as root, assuming you want to install for user lispservice
# must have quicklisp already installed
# cd ~
# ftp https://beta.quicklisp.org/quicklisp.lisp 
# sbcl
# (load "quicklisp.lisp")
# follow directions at prompt and add to init file when necessary
# note that this CANNOT be run all at once, these are just instructions for future use I suppose
_serverinstall {
	su lispservice
	cd ~
	mkdir common-lisp
	cd common-lisp
	git clone https://gitlab.com/vancan1ty/cl-berryutils berryutils
	git clone https://gitlab.com/vancan1ty/dumb-mq dumb-mq
	# at this point in time, boot up sbcl and use quicklisp to download all the deps
	# (e.g. in sbcl run (ql:quickload :berryutils) (ql:quickload :berrysync) )
i	# once you have successfully loaded berrysync, move on to the next step
			
	# put the dmqservice directory inside of the /service directory
	# make sure the main directory under log is owned by the user you are running it as
	# daemontools should start it up

	exit
}


eval "$1" "$2"

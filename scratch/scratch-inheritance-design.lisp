(require :berryutils)
(ql:quickload :closer-mop)

(defpackage scratch
  (:use :common-lisp :berryutils)
  )

(in-package :scratch)


(defstruct dmq-message
  (protocol-version) ;0-9
  (sequence-number) ;6 digits
  (total-message-size) ;in bytes, including header
  (fncode) ; standardized location for three character function code
  )

(defstruct berrysync-onchange-message (dmq-message)
           (folder-name))

(defparameter *tmessage* (make-berrysync-onchange-message :dmq-message (make-dmq-message :protocol-version 0 :sequence-number 12 :total-message-size 20 :fncode "CHN") :folder-name "/foo/bar"))

;; CB -- so verbose!
(dmq-message-sequence-number (berrysync-onchange-message-dmq-message *tmessage*))
;;*tmessage*.sequence-number

;; nonstandard, but a bit less verbose
(slot-value (slot-value *tmessage* 'dmq-message) 'sequence-number)

;; CB one improvement is to ditch the prefixes
;; that is what packages are for dummy!!!!


(defstruct message
  (protocol-version) ;0-9
  (sequence-number) ;6 digits
  (total-message-size) ;in bytes, including header
  (fncode) ; standardized location for three character function code
  )

(defstruct onchange-message (message)
           (folder-name))

(defparameter *tmessage* (make-onchange-message :message (make-message :protocol-version 0 :sequence-number 12 :total-message-size 20 :fncode "CHN") :folder-name "/foo/bar"))

;; CB -- not so bad!
(message-sequence-number (onchange-message-message *tmessage*))
;*tmessage*.sequence-number

;; nonstandard
(slot-value (slot-value *tmessage* 'message) 'sequence-number)

(defclass cmessage ()
  ((protocol-version :initarg protocol-version)
   (sequence-number :initarg sequence-number)
   (total-message-size :initarg total-message-size)
   (fncode :initarg fncode)))

(defclass onchange-cmessage (cmessage)
  ((folder-name :initarg folder-name)
   (special-var)))

(defparameter *cmessage* (make-instance 'onchange-cmessage 'protocol-version 0 'sequence-number 12 'total-message-size 20 'fncode "CHN" 'folder-name "foo/bar" 'special-var 44))


(make-instance 'onchange-cmessage 'special-var 45)
;*cmessage*
;(mapcar #'sb-mop:slot-definition-name (sb-mop:class-slots (class-of *cmessage*)))

(setup-object-printing)

(slot-value *cmessage* 'special-var)


(format t "~a" (print-object *cmessage* t))

;;(fmakunbound 'print-object)

(defparameter *cmessage2* (read-from-string  "{ ONCHANGE-CMESSAGE ((PROTOCOL-VERSION . 0) (SEQUENCE-NUMBER . 12)
                     (TOTAL-MESSAGE-SIZE . 20) (FNCODE . \"CHN\")
                     (FOLDER-NAME . \"foo/bar\"))}"))

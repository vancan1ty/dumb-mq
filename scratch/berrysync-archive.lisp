;; the place where code goes to die

(defun start-simple-server (port)
  "Listening on a port for a message, and print the received message."
  (usocket:with-socket-listener (socket "127.0.0.1" port)
    (usocket:wait-for-input socket)
    (usocket:with-connected-socket (connection (usocket:socket-accept socket))
      (let ((line (read-line (usocket:socket-stream connection))))
        (loop while (not (eql line (string #\q))) do
             (format t "~a~%" line)
             (setf line (read-line (usocket:socket-stream connection))))))))

(defun start-simple-client (port)
  "Connect to a server and send a message."
  (usocket:with-client-socket (socket stream "127.0.0.1" port)
    (format stream "Hello world!~%how are you today?~%q~%")
    (force-output stream)))

;(start-simple-server 5445)
;(start-simple-client 5445)


;; (defun foo()
;;   (let* ((mprocessobj (sb-ext:run-program  "/usr/bin/git" (list "push" "origin" "master") :pty t :directory "/testcases/berrysync_test_node2"))
;;          (mprocess-output (sb-ext:process-error mprocessobj)))
;;     (format t "here 1~%")
;;     (format t "yah ~A~%" (read-char-no-hang mprocess-output))
;;     (format t "here 2~%")
;;     ))

;; (defun foo()
;;   (let* ((mprocessobj (sb-ext:run-program  "/usr/bin/git" (list  "push" "origin" "master") :pty t :directory "/testcases/berrysync_test_node3"))
;;          (mprocess-output (sb-ext:process-pty mprocessobj)))
;;     (format t "here 1~%")
;;     (format t "yah ~A~%" (read-output-stream-to-string mprocess-output))
;;     (format t "here 2~%")
;;     ))


    ;; the below command will be useful in the future!
    ;;"git ls-files -omt --exclude-standard"


;; ;;sbcl run program example
;; (sb-ext:run-program  "/usr/bin/git" (list "status") :pty *standard-output*)
;; (multiple-value-bind (code results) (run-git "." (list "status"))
;;   (format t "code: ~s~%results: ~s~%" code results))
;; (cons "status" (list "a" "b" "c"))

;; (defmacro debug-if (a b &optional c)
;;   `(progn
;;      (format t "debug-if invoked with the following arguments: try-code: ~s, success ~s~%" try-code 'success)
;;      (if ,a
;;          ,b
;;          ,c)))



(let ((a 1)
      (b 2))
  (multiple-value-bind (a b) (lambda () (values 3 4)))
  (format t "a: ~s, b: ~s~%" a b))

(defun handle-push-failure (folder)
  "follows the directions layed out in the berrysync-start-node-server comments"

  )


(defun send-keep-alives-to-everybody ()
  (declare (optimize (debug 3)))
  (let ((address-to-mseq-map (make-hash-table :test 'equalp))
        (time-remaining *keep-alive-query-time*))
    (do ((i 1)) ((< i (length *open-sockets-list*)))
      (let* ((socket (elt *open-sockets-list* i))
             (address (usocket:get-peer-address socket))
             (mstream (usocket:socket-stream socket))
             (mseq (random 1000)))
        (setf (gethash address *address-to-mseq-map*)
              mseq)
        (format mstream "k ~a~%" mseq)
        (force-output mstream) ;;flush the output
        ))
    (do () ((> time-remaining 0))
      (let (readsockets)
        (setf (values readsockets time-remaining) (usocket:wait-for-input (subseq *open-sockets-list* 1) :read-only t :timeout time-remaining))
        (dolist (rsocket readsockets)
          (handler-case 
              (let ((line-from-client (stream-read-line (usocket:socket-stream rsocket)))
                    (raddress (usocket:get-peer-address rsocket))
                    actualcommand expectedseq actualseq)
                (if (eql (length line-from-client) 0) ;;then I believe this means that the connection is closed
                    (progn
                      (format t "Connection shut down by remote ~a~%" raddress)
                      (delete socket *open-sockets-list*)
                      (return-from process-socket-wake-up)))
                (setq expectedseq (gethash raddress address-to-mseq-map))
                (setq actualcommand (elt line-from-client 0))
                (setq actualseq (parse-integer (subseq line-from-client 2)))
                (when (eql actualcommand #\a))
                (if (equal actualseq expectedseq)
                    (remhash raddress address-to-mseq-map)
                    (progn
                      (format *error-output* "ERROR: incorrect seq response from ~a~%" raddress)
                      (delete rsocket *open-sockets-list*))))
            (error (input)
              (format t "ERROR: ~a~%" input)
              (delete rsocket *open-sockets-list*)))
          )))))



(defun increment-seq-number ()
  (sb-thread:with-mutex (*seq-number-lock*)
    (setf *current-sequence-number* (mod (+ *current-sequence-number* 1) 1000000))
    *current-sequence-number*))

(defmethod submit-uow-to-dest ((uow uow) dest)
  (cond 
    ((typep dest 'queues:simple-queue)
     (queues:qpush dest uow))
    ((typep dest 'function)
     (funcall dest uow))
    (t
     (v:error "could not submit uow to dest ~s" dest))))


(in-package :berrysync)
;;CB this file contains the abstract definition of the plugin api
;; actual plugins should subclass this, and implement the methods 

;;CB TODO add advertising messages so nodes can query each other to see what
;;they support
(defclass dmq-plugin ()
  ((plugin-id :accessor plugin-id :initform nil
              :documentation "should be supplied by initialize-instance :after")
   (dmq-instance :accessor plugin-dmq-instance :initform nil
                 :documentation "usually injected by post-setup-plugin"))
  (:documentation "your plugin class should extend this and implement its methods"))

(defgeneric pre-setup-plugin (plugin dmq)
  (:documentation "performs initialization procedures for the plugin"))
(defgeneric post-setup-plugin (plugin)
  (:documentation "performs initialization procedures for the plugin (plugin has already been added to the handler list)"))
(defgeneric destroy-plugin (plugin)
  (:documentation "shuts down a plugin"))
(defgeneric process-uow (plugin uow)
  (:documentation "plugins must implement this.  plugin is the dmq-plugin class representing the plugin.  returns value is disregarded -- if you want to send back to the other side you need to call the relevant method."))
(defgeneric on-endpoint-shutdown (plugin endpoint)
  (:documentation "called to notify the plugin that a connection has been terminated"))
(defgeneric on-endpoint-connect (plugin endpoint)
  (:documentation "called to notify the plugin that a connection has been established"))
(defgeneric provide-handled-fncodes (plugin)
  (:documentation
   "returns a list of all the fncodes which this plugin is responsible for handling"))

(defmethod provide-handled-fncodes ((plugin dmq-plugin))
  "empty implementation"
  ())

(defmethod pre-setup-plugin ((this dmq-plugin) dmq)
  "empty implementation"
  (setf (plugin-dmq-instance this) dmq))

(defmethod post-setup-plugin ((plugin dmq-plugin))
  "empty implementation"
  ())
(defmethod destroy-plugin (plugin)
  "empty implementation"
  ())

(defmethod process-uow ((plugin dmq-plugin) uow)
  "empty implementation"
  ())

(defmethod on-endpoint-connect ((plugin dmq-plugin) endpoint)
  "empty implementation"
  ())

(defmethod on-endpoint-shutdown ((plugin dmq-plugin) endpoint)
  "empty implementation"
  ())


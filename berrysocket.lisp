(in-package :usocket)

(defgeneric socket-short-rep (socket))

(defclass berry-stream-usocket (usocket:stream-usocket)
  ((socket-id :accessor get-socket-id :initform nil :initarg socket-id))
 (:documentation "like a stream-usocket, but persists a socket-id 
contains the remote host and port even after the socket is disconnected."))

(defun wrap-in-berry-stream-usocket (stream-usocket)
  "pass in a connected stream-usocket, and this will return a berry-stream-usocket"
  (change-class stream-usocket 'berry-stream-usocket 'socket-id (socket-short-rep stream-usocket))
;;  #+sbcl (setf (sb-bsd-sockets:non-blocking-mode (slot-value stream-usocket 'usocket::socket)) t)
;;  #+sbcl (assert (eql t (sb-bsd-sockets:non-blocking-mode (slot-value stream-usocket 'usocket::socket))))
  stream-usocket
  )

(defmethod socket-short-rep ((socket usocket:stream-usocket))
  "this short representation is used to identify endpoints as well as sockets"
  (handler-bind
      ((error #'(lambda (data)
                  (v:error :socketer "error ~a getting info on ~a" data socket)
                  (invoke-debugger data)
                  )))
      (format nil "~a:~a" (usocket:get-peer-address socket)  (usocket:get-peer-port socket))))

(defclass berry-server-usocket (usocket:stream-server-usocket)
  ((socket-id :accessor get-socket-id :initform nil :initarg socket-id))
 (:documentation "like a stream-server-usocket, but persists a socket-id 
contains the remote host and port even after the socket is disconnected."))

(defun wrap-in-berry-stream-server-usocket (server-usocket)
  "pass in a connected stream-server-usocket, and this will return a berry-server-usocket"
  (change-class server-usocket 'berry-server-usocket 'socket-id (socket-short-rep server-usocket)))

(defmethod socket-short-rep ((socket usocket:stream-server-usocket))
  "this short representation is used to identify endpoints as well as sockets"
  (format nil "~a" socket))

(export (list 'wrap-in-berry-stream-usocket 'wrap-in-berry-stream-server-usocket 'socket-short-rep 'berry-stream-usocket 'berry-server-usocket 'get-socket-id))



;; based on wait-for-input found in usocket.lisp
(defun wait-for-output (socket-or-sockets &key timeout ready-only)
  (when (null socket-or-sockets)
    (when timeout
      (sleep timeout))
    (return-from wait-for-output nil))

  (unless (wait-list-p socket-or-sockets)
    (let ((wl (make-wait-list (if (listp socket-or-sockets)
                                  socket-or-sockets (list socket-or-sockets)))))
      (multiple-value-bind
            (socks to)
          (wait-for-output wl :timeout timeout :ready-only ready-only)
        (return-from wait-for-output
          (values (if ready-only socks socket-or-sockets) to)))))

  (let* ((start (get-internal-real-time))
         (sockets-ready 0))
    ;; (dolist (x (wait-list-waiters socket-or-sockets))
    ;;   (when (setf (state x)
    ;;               #+(and win32 (or sbcl ecl)) nil ; they cannot rely on LISTEN
    ;;               #-(and win32 (or sbcl ecl))
    ;;               (if (and (stream-usocket-p x)
    ;;                        (listen (socket-stream x)))
    ;;                   :read
    ;;                   nil))
    ;;     (incf sockets-ready)))
    ;; the internal routine is responsibe for
    ;; making sure the wait doesn't block on socket-streams of
    ;; which theready- socket isn't ready, but there's space left in the
    ;; buffer
    (wait-for-output-internal socket-or-sockets
                             :timeout (if (zerop sockets-ready) timeout 0))
    (let ((to-result (when timeout
                       (let ((elapsed (/ (- (get-internal-real-time) start)
                                         internal-time-units-per-second)))
                         (when (< elapsed timeout)
                           (- timeout elapsed))))))
      (values (if ready-only
                  (remove-if #'null (wait-list-waiters socket-or-sockets) :key #'state)
                  socket-or-sockets)
              to-result))))


#+sbcl
(defun wait-for-output-internal (sockets &key timeout)
  (with-mapped-conditions ()
    (sb-alien:with-alien ((wfds (sb-alien:struct sb-unix:fd-set)))
      (sb-unix:fd-zero wfds)
      (dolist (socket (wait-list-%wait sockets))
        (sb-unix:fd-set
         (sb-bsd-sockets:socket-file-descriptor socket)
         wfds))
      (multiple-value-bind
            (secs musecs)
          (split-timeout (or timeout 1))
        (multiple-value-bind
              (count err)
            (sb-unix:unix-fast-select
             (1+ (reduce #'max (wait-list-%wait sockets)
                         :key #'sb-bsd-sockets:socket-file-descriptor))
             nil (sb-alien:addr wfds) nil
             (when timeout secs) (when timeout musecs))
          (if (null count)
              (unless (= err sb-unix:EINTR)
                (error (map-errno-error err)))
              (when (< 0 count)
                ;; process the result...
                (dolist (x (wait-list-waiters sockets))
                  (when (sb-unix:fd-isset
                         (sb-bsd-sockets:socket-file-descriptor
                          (socket x))
                         wfds)
                    (setf (state x) :WRITE))))))))))

(in-package :berrysync)
(declaim (optimize (debug 3) (safety 3) (speed 0)))

;;dmq-server-config
(defclass dmq-server-config (dmq-config)
  ())
           


(defun run-server (server-config server-state)
  "waits for changes on *open-sockets-list*, and periodically tests sockets to make sure they are still open.
   after calls to wait-for-input, loops through last-read-time-map, and sends out keepalives if necessary
   also accepts new connection requests"
  (declare (optimize (debug 3)))
  (let ()
    (setf (dmq-server-state-central-server-socket server-state)  (usocket:socket-listen usocket:*wildcard-host* *accept-new-connections-port* :reuse-address t))
    (setf (dmq-server-state-open-sockets-list server-state) (append (list *central-server-socket*) *open-sockets-list*)) ;;add the server socket
    (unwind-protect
         (progn
           (do () (())
             (handler-case 
                 (progn
                   (format t "hi~%")
                   (let (readable-sockets)
                     (setf readable-sockets
                           (usocket:wait-for-input (dmq-server-state-open-sockets-list server-state) :ready-only t :timeout *wait-timeout*))
                     (when (not (eql readable-sockets nil)) ;then probably the call timed out 
                       (loop for socket in readable-sockets do
                            (handler-case
                                (process-socket-wake-up socket)
                              (error (data)
                                (format t "ERROR: ~a while reading from socket ~a~%" data socket)
                                (usocket:socket-close socket)
                                (delete socket (dmq-server-state-open-sockets-list server-state))))))
                     (maphash
                      (lambda (key value)
                        (handler-case
                            (let ((time-difference-ms (- (get-internal-real-time) value)))
                              (cond
                                ((> time-difference-ms
                                    (* 1000 (dmq-server-config-keepalive-deletion-threshold server-config ))
                                 (remhash key *last-read-time-map*)))
                                ((> time-difference-ms
                                    (* 1000 (dmq-server-config-keepalive-transmission-threshold server-config)))
                                 (send-keepalive key))
                                (t nil) ; do nothing
                                ))
                          (error (data)
                            (format t "ERROR: ~a while performing keepalive actions to socket ~a~%" data key)
                            (usocket:socket-close key)
                            (delete key *open-sockets-list*))))
                      *last-read-time-map*
                      )))
               (error (data) (format t "ERROR:h ~a~%" data))
               )))
      (progn
        (format t "closing *central-server-socket*")
        (loop for socket in *open-sockets-list* do
             (usocket:socket-close socket))
        (setf *open-sockets-list* nil)
        (clrhash *last-read-time-map*)) 
      )))

(defun process-socket-wake-up (socket)
  (cond ;;switch on socket type
    ((typep socket 'usocket:stream-server-usocket) ;;then make a new socket and add it to our list!
     (let ((nsocket (usocket:socket-accept socket)))
       (setq *open-sockets-list* (append *open-sockets-list* (list nsocket)))
       (setf (gethash nsocket *last-read-time-map*)
             (get-internal-real-time))))

    ((typep socket 'usocket:stream-usocket) ;; read the input from the socket
     (let ((line-from-client (read-line (usocket:socket-stream socket))) data)
       (format t "INFO: read ~s from ~a~%" line-from-client socket)
       (if (eql (length line-from-client) 0) ;;then I believe this means that the connection is closed
           (progn
             (format t "Connection shut down by remote ~a~%" (usocket:get-peer-address socket))
             (delete socket *open-sockets-list*)
             (return-from process-socket-wake-up)))
       (setf (gethash socket *last-read-time-map*)
             (get-internal-real-time))
       (cond ;;switch on command from client
         ((eql (elt line-from-client 0) #\c) ;;folder change detected...
          (setq data (subseq line-from-client 2))
          (send-out-folder-update socket data))
         ((eql (elt line-from-client 0) #\a) ;;keepalive ack
          nil)
         ((eql (elt line-from-client 0) #\k) ;;keepalive 
          (keepalive-reply socket))
         (t
          (format *error-output* "ERROR: unsupported command ~a~%" (elt line-from-client 0))))))

    (t
     (format t "ERROR: unsupported socket type for argument ~a~%" socket))
    ))

;; k stands for keepalive
(defun send-keepalive (socket)
  (format t "INFO: sending keepalive to ~a~%" socket)
  (format (usocket:socket-stream socket) "k~%")
  (force-output (usocket:socket-stream socket)))

(defun keepalive-reply (socket)
  (format t "INFO: sending keepalive reply to ~a~%" socket)
  (format (usocket:socket-stream socket) "a~%")
  (force-output (usocket:socket-stream socket)))

(defun bsock-addr (usocket)
  (usocket:get-peer-address usocket))

;(run-server)
;;(defparameter *tmessage* (make-berrysync-onchange-message :dmq-message (make-dmq-message :protocol-version 0 :sequence-number 12 :total-message-size 20 :fncode "CHN") :folder-name "/foo/bar"))
;;(dmq-message-sequence-number (berrysync-onchange-message-dmq-message *tmessage*))
;;*tmessage*.sequence-number


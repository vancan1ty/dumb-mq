(in-package :berrysync)

(defparameter *fs-format-str*  "~a")

(defstruct fs-onchange-message (dmq-message)
           (folder-name))

(defun fs-onchange-message-to-string () ())


(defun read-string-to-message (str)
  (let ((protocol-version (parse-integer (subseq str 0 1)))
        (sequence-number (parse-integer (subseq str 2 8)))
        (message-size (parse-integer (subseq str 9 15)))
        (fncode (subseq str 16 19))
        (body (if (> (length str) 20) (subseq str 20) "")))
    (make-instance 'message 'protocol-version protocol-version 'sequence-number sequence-number 'total-message-size message-size 'fncode fncode 'body body)))

(defun message-to-string (message)
  (concatenate 'string
               (format nil *message-format-str*
                       (slot-value message 'protocol-version)
                       (slot-value message 'sequence-number)
                       (slot-value message 'total-message-size)
                       (slot-value message 'fncode))
               (if (and (slot-value message 'body) (> (length (slot-value message 'body)) 0))
                   (concatenate 'string "_" (slot-value message 'body)))))

(defclass message ()
  ((protocol-version :initarg protocol-version :initform 0) ;0-9
   (sequence-number :initarg sequence-number :documentation "6 digits") ;6 digits
   (total-message-size :initarg total-message-size :documentation "in bytes, including header") 
   (fncode :initarg fncode) ; standardized location for three character function code
   (body :initarg body) ; the message contents, which you can handle however you like
   ))


(defun calculate-message-size (message)
  (length (message-to-string message)))



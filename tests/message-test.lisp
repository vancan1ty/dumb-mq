(in-package :berrysync.tests)

(def-suite message :in berrysync)
(in-suite message)

(defparameter p1str "0_123456_000343_CHN")
(defparameter p2str "0_123456_434343_CHN_")
(defparameter p2str-alt "0_123456_434343_CHN")
(defparameter p3str "0_123456_434343_CHN_d")
;;(slot-value (read-string-to-message "0_123456_000343_CHN") 'berrysync::protocol-version)


(test read-string-to-message
      (let ((p1 (read-string-to-message "0_123456_000343_CHN"))
            (p2 (read-string-to-message "0_123456_434343_CHN_"))
            (p3 (read-string-to-message "0_123456_434343_CHN_d")))
        (is (equal (slot-value p1 'berrysync::protocol-version) 0))
        (is (equal (slot-value p1 'berrysync::sequence-number) 123456))
        (is (equal (slot-value p1 'berrysync::total-message-size) 343))
        (is (equal (slot-value p1 'berrysync::fncode) "CHN"))
        (is (equal (slot-value p1 'berrysync::body) ""))
        (is (equal (slot-value p2 'berrysync::body) ""))
        (is (equal (slot-value p2 'berrysync::body) ""))
        (is (equal (slot-value p3 'berrysync::body) "d"))))

(test message-to-string
      (let ((p1 (read-string-to-message p1str))
            (p2 (read-string-to-message p2str))
            (p3 (read-string-to-message p3str)))
        (is (equal (message-to-string p1) p1str))
        (is   (equal (message-to-string p2) p2str-alt))
        (is    (equal (message-to-string p3) p3str))))

(defparameter t_invalidstr1 "0*123456_000343_CHN")
(defparameter t_invalidstr2 "0*123456_000343_CH+")

(test validate-message-header
  (is (berrysync::validate-message-header p1str))
  (is (not (berrysync::validate-message-header t_invalidstr1)))
  (is (not (berrysync::validate-message-header t_invalidstr2))))

;;(fiveam:run! 'message)

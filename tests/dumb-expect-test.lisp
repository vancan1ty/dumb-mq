;; not yet ported to fiveAM
(in-package :berrysync.tests)

(defun make-password-prompt-provider (timebeforewrite)
  (lambda (server-to-client-stream) (bt:make-thread (lambda ()
                                                      (format t "starting password provider~%")
                                                      (sleep (/ timebeforewrite 1000))
                                                      (if (open-stream-p server-to-client-stream)
                                                          (progn
                                                            (format t "writing password~%")
                                                            (format server-to-client-stream "Password:"))
                                                          (format t "not writing password because stream has closed!~%"))))))

(defun make-tricky-password-prompt-provider (timebeforefirstwrite timebeforesecondwrite)
  (lambda (server-to-client-stream)
    (bt:make-thread (lambda ()
                      (format t "starting password provider~%")
                      (sleep (/ timebeforefirstwrite 1000))
                      (if (open-stream-p server-to-client-stream)
                          (progn
                            (format t "writing pass~%")
                            (format server-to-client-stream "Pass"))
                          (progn
                            (format t "not writing pass because stream has closed!~%")
                            (return-from make-tricky-password-prompt-provider :stream-closed)))
                      (sleep (/ timebeforesecondwrite 1000))
                      (if (open-stream-p server-to-client-stream)
                          (progn
                            (format t "writing word~%")
                            (format server-to-client-stream "word:"))
                          (progn
                            (format t "not writing word because stream has closed!~%")
                            (return-from make-tricky-password-prompt-provider :stream-closed)))))))

(defmacro make-dumb-expect-test (&rest body)
  `(let (
         (us-to-server-stream (make-string-output-stream))
         (server-to-us-stream (make-string-output-stream))
         )
     (unwind-protect 
          (progn ,@body)
       (close us-to-server-stream)
       (close server-to-us-stream))))

(defun make-successful-dumb-expect-runner (prompt-provider timeout response-provider)
  (lambda ()
    (make-dumb-expect-test
     (funcall prompt-provider server-to-us-stream)
     (let ((result
            (handler-case 
                (dumb-expect timeout server-to-us-stream us-to-server-stream
                             response-provider)
              (simple-error (data)
                :timeout-failure))))
       (check (print-transparent (equal result :success)))
       (check (equal (get-output-stream-string us-to-server-stream) "mypass
"))
       result))))

(defun successful-dumb-expect-run()
  (funcall
   (make-successful-dumb-expect-runner
    (make-password-prompt-provider 1000)
    1100
    (berrysync:make-password-response-provider))))

(defun successful-tricky-dumb-expect-run()
  (funcall
   (make-successful-dumb-expect-runner
    (make-tricky-password-prompt-provider 300 600)
    1100
    (make-password-response-provider))))

(defun unsuccessful-dumb-expect-run-w-timeout ()
  (make-dumb-expect-test
   (let ((pass-provider (make-password-prompt-provider 5000)))
     (funcall pass-provider server-to-us-stream)
     (let ((result
            (handler-case 
                (dumb-expect 3000 server-to-us-stream us-to-server-stream
                             (make-password-response-provider))
              (simple-error (data)
                :timeout-failure))))
       (check (equal result :timeout-failure))
       result))))

(deftest test-dumb-expect ()
  (successful-dumb-expect-run)
  (unsuccessful-dumb-expect-run-w-timeout)
  (successful-tricky-dumb-expect-run)
  )


(in-package :berrysync.tests)

(def-suite berrysync-core :in berrysync)
(in-suite berrysync-core)

(defparameter *primary-test-repo-location* "/testcases/berrysync_test_node")

(defun make-password-prompt-provider (timebeforewrite)
  (lambda (server-to-client-stream) (bt:make-thread (lambda ()
                                                      (format t "starting password provider~%")
                                                      (sleep (/ timebeforewrite 1000))
                                                      (if (open-stream-p server-to-client-stream)
                                                          (progn
                                                            (format t "writing password~%")
                                                            (format server-to-client-stream "Password:"))
                                                          (format t "not writing password because stream has closed!~%"))))))

(test git-commit-push ()
      (with-open-file (outfile (concatenate 'string *primary-test-repo-location*  "/test.txt") :direction :output :if-exists :append)
        (format outfile "some data from lisp~%"))
      ;;now commit
      (multiple-value-bind (code data)
          (run-git *primary-test-repo-location* (list "commit" "-a" "--message" (concatenate 'string "test-git-commit-push " (machine-instance))))
        (is (eql code 0))
        (is (not (eql nil (search "changed" data)))))
      ;;now push
      (is (eql :success (git-push *primary-test-repo-location*))))

;(test-git-commit-push)

(test git-pull ()
  (is (eql :success (git-pull *primary-test-repo-location*))))

;(test-git-pull)

(test read-output-stream-to-string ()
  (let ((istream (make-string-input-stream "this is the data")))
    (unwind-protect 
         (let ((read-data (read-output-stream-to-string istream)))
           (is (equal read-data "this is the data")))
      (close istream))))

;(test-read-output-stream-to-string)
;(vector-push-extend
; (sb-ext:process-status-hook 

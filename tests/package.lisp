(defpackage :berrysync.tests
  (:use #:cl #:alexandria #:fiveam #:berryutils #:berrysync)
  (:export #:run-tests))

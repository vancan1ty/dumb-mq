(defparameter *clientsocket* nil)
(defun berry-start-client (port)
  (setf *clientsocket* (make-instance 'sb-bsd-sockets:inet-socket :type :stream :protocol :tcp))
  (sb-bsd-sockets:socket-connect *clientsocket* '(127 0 0 1) port))

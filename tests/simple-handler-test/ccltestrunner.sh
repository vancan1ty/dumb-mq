#!/bin/sh

s1 () {
	( cd ../../ && /projects/ccl/scripts/ccl64 --eval '(load "./tests/simple-handler-test/s1config.lisp")' )
}

c1 () {
	( cd ../../ && /projects/ccl/scripts/ccl64 --eval '(load "./tests/simple-handler-test/c1config.lisp")' )
}

c2 () {
	( cd ../../ && /projects/ccl/scripts/ccl64 --eval '(load "./tests/simple-handler-test/c2config.lisp")' )
}

eval $1 $2 $3

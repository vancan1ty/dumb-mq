(require :asdf)
(asdf:load-system :berrysync)

(in-package :berrysync)

(defparameter *my-dmq*
  (setup-dmq
   (make-instance 'dmq-config 'listen-port 9090
                  'plugins (list (make-instance 'simple-handler-server)))))

 (start-dmq *my-dmq* t)

(asdf:load-system :berrysync)

(in-package :berrysync)

(defparameter *my-dmq*
  (setup-dmq
   (make-instance 'dmq-config 
                  'plugins (list (make-instance 'simple-handler-client
                                                'ip-and-port-to-connect-to (cons "127.0.0.1" 9090))))))
 (start-dmq *my-dmq* nil)

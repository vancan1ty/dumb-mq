#!/bin/sh

s1 () {
	( cd ../../ && sbcl --eval '(load "./tests/simple-handler-test/s1config.lisp")' )
}

c1 () {
	( cd ../../ && sbcl --eval '(load "./tests/simple-handler-test/c1config.lisp")' )
}

c2 () {
	( cd ../../ && sbcl --eval '(load "./tests/simple-handler-test/c2config.lisp")' )
}

eval $1 $2 $3

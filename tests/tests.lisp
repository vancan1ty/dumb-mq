;; based on serapeum testing configuration
;; https://github.com/TBRSS/serapeum
(in-package #:berrysync.tests)

(def-suite berrysync)
(in-suite berrysync)

(defun run-tests ()
  (5am:run! 'berrysync))

(defun run-tests/quiet ()
  (handler-bind ((warning #'muffle-warning))
    (run-tests)))

(defun debug-test (test)
  (let ((5am:*debug-on-error* t)
        (5am:*debug-on-failure* t))
    (run! test)))

(defun a-fixnum ()
  (lambda ()
    (random-in-range most-negative-fixnum most-positive-fixnum)))

(defun an-iota (n)
  (lambda ()
    (iota n)))

(defun a-list-of (len fn)
  (lambda ()
    (map-into (make-list len) fn)))

(defun eval* (form)
  "Variant of eval forcing macroexpansion."
  (funcall (compile nil (eval `(lambda () ,form)))))

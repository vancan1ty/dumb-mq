(in-package :berrysync.tests)

(defun list-existent-directory-on-remote-server ()
  (let ((program "/bin/ls")
        (directory "/home/vancan1ty")
        (argslist '("-l")))
  (let ((prog-to-us-stream nil)
        (process-object nil))
    (setf prog-to-us-stream (make-string-output-stream))
    (unwind-protect
         (progn
           (setf process-object (sb-ext:run-program program argslist :pty prog-to-us-stream :directory directory))
           (values (sb-ext:process-exit-code process-object) (get-output-stream-string prog-to-us-stream)))
      (close prog-to-us-stream))))


(defun run-program (program directory argslist)
  (let ((prog-to-us-stream nil)
        (process-object nil))
    (setf prog-to-us-stream (make-string-output-stream))
    (unwind-protect
         (progn
           (setf process-object (sb-ext:run-program program argslist :pty prog-to-us-stream :directory directory))
           (values (sb-ext:process-exit-code process-object) (get-output-stream-string prog-to-us-stream)))
      (close prog-to-us-stream))))

(run-program "/bin/ls" "/home/vancan1ty" '("-l"))

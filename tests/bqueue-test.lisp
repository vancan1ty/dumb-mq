(in-package :berrysync.tests)

(def-suite mqueues :in berrysync)
(in-suite mqueues)

(test super-queue 
      (let ((q (queues:make-queue :super-cqueue)))
        (queues:qpush q 101)
        (is (equal (queues:qsize q) 1))
        (queues:qpop q)
        (is (equal (queues:qsize q) 0))))

(test mt-super-queue
      (let ((q (queues:make-queue :super-cqueue)))
        (bt:make-thread (lambda ()
                          (sleep 3.0)
                          (queues:qpush q 404)
                          (sleep 0.1) ;;unfortunately it looks like, without this sleep, the other thread is not guaranteed to pop the item but this one will instead.  oh well, nbd for what I am doing.
                          (assert (eql nil (queues:qpop q))) ;;if this is working right, this should return nothing
                          (queues:qpush q 505)))
        (format t "heaaa~%")
        (is (eql 404 (queues:qpop-or-wait q)))
        (is (eql 505 (queues:qpop-or-wait q)))))


(fiveam:run! 'mqueues)


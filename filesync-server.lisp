(require :usocket)
(require :alexandria)
(require :berryutils)
(defpackage berrysync-server
  (:use :common-lisp)
  (:export
   run-server
   ))

(in-package :berrysync-server)

(declaim (optimize (debug 3) (safety 3) (speed 0)))

;; this thing hosts a socket server
;; nodes connect to the socket this thing hosts
;; then from then on, the socket is used as a bidirectional channel of communication
;; whenever a client pushes an update to the server,
;;    then the server will push out notifications to all the other connected clients
;; whenever a client first comes online, it is expected to first connect to the server,
;; then use "git pull" to get the most recent changes which may have occurred while it was offline

(defparameter *central-server-socket* nil)

(defparameter *accept-new-connections-port* 4040)

;; (defun berrysync-start-central-server ()
;;   "starts up a central server to wait for modification notifications from clients"
;;   (setf *central-server-socket* (make-instance 'sb-bsd-sockets:inet-socket 

;; the server runs in one thread
;; if the thread detects a closed connection on recv, it removes it from the list
;; after the select operation occurs or expires, each time we check against a timeout (10 minutes, let's say) for polling all of the sockets in open-sockets-list to see if they still work.  if we don't get a response in 30 seconds for a given socket, then we drop it.

(defparameter *open-sockets-list* ())

;;(defparameter *wait-timeout* (* 60 10))
(defparameter *wait-timeout* (* 60 1))

;; if we haven't heard from a client in 30 minutes, send it a keepalive!
;;(defparameter *keepalive-transmission-threshold* (* 60 30))
(defparameter *keepalive-transmission-threshold* (* 60 3))

;; if we haven't heard from a client in over an hour, delete it!
;;(defparameter *keepalive-deletion-threshold* (* 60 62))
(defparameter *keepalive-deletion-threshold* (* 60 7))

(defparameter *last-read-time-map* (make-hash-table :test 'eql)
  "maps sockets to the last time we explicitly read from them")

;; k stands for keepalive
(defun send-keepalive (socket)
  (format t "INFO: sending keepalive to ~a~%" socket)
  (format (usocket:socket-stream socket) "k~%")
  (force-output (usocket:socket-stream socket)))

(defun send-out-folder-update (originating-socket folder-id)
  (do ((i 1)) ((>= i (length *open-sockets-list*))) 
    (let* ((socket (elt *open-sockets-list* i)))
      (when (not (eql socket originating-socket)) ;;then queue up an update for this client!
        (format t "INFO: sending \"c ~a\" to ~a ~%" folder-id socket)
        (format (usocket:socket-stream socket) "c ~a~%" folder-id)
        (force-output (usocket:socket-stream socket))))
    (incf i)))

(defun keepalive-reply (socket)
  (format t "INFO: sending keepalive reply to ~a~%" socket)
  (format (usocket:socket-stream socket) "a~%")
  (force-output (usocket:socket-stream socket)))

(defun process-socket-wake-up (socket)
  (cond ;;switch on socket type

    ((typep socket 'usocket:stream-server-usocket) ;;then make a new socket and add it to our list!
     (let ((nsocket (usocket:socket-accept socket)))
       (setq *open-sockets-list* (append *open-sockets-list* (list nsocket)))
       (setf (gethash nsocket *last-read-time-map*)
             (get-internal-real-time))))

    ((typep socket 'usocket:stream-usocket) ;; read the input from the socket
     (let ((line-from-client (read-line (usocket:socket-stream socket))) data)
       (format t "INFO: read ~s from ~a~%" line-from-client socket)
       (if (eql (length line-from-client) 0) ;;then I believe this means that the connection is closed
           (progn
             (format t "Connection shut down by remote ~a~%" (usocket:get-peer-address socket))
             (delete socket *open-sockets-list*)
             (return-from process-socket-wake-up)))
       (setf (gethash socket *last-read-time-map*)
             (get-internal-real-time))
       (cond ;;switch on command from client
         ((eql (elt line-from-client 0) #\c) ;;folder change detected...
          (setq data (subseq line-from-client 2))
          (send-out-folder-update socket data))
         ((eql (elt line-from-client 0) #\a) ;;keepalive ack
          nil)
         ((eql (elt line-from-client 0) #\k) ;;keepalive 
          (keepalive-reply socket))
         (t
          (format *error-output* "ERROR: unsupported command ~a~%" (elt line-from-client 0))))))

    (t
     (format t "ERROR: unsupported socket type for argument ~a~%" socket))
    ))

(defun bsock-addr (usocket)
  (usocket:get-peer-address usocket))

(defun run-server ()
  "waits for changes on *open-sockets-list*, and periodically tests sockets to make sure they are still open.
   after calls to wait-for-input, loops through last-read-time-map, and sends out keepalives if necessary
   also accepts new connection requests"
  (declare (optimize (debug 3)))
  (let ()
    (setf *central-server-socket* (usocket:socket-listen usocket:*wildcard-host* *accept-new-connections-port* :reuse-address t))
    (setq *open-sockets-list* (append (list *central-server-socket*) *open-sockets-list*)) ;;add the server socket
    (unwind-protect
         (progn
           (do () (())
             (handler-case 
                 (progn
                   (format t "hi~%")
                   (let (readable-sockets)
                     (setf readable-sockets
                           (usocket:wait-for-input *open-sockets-list* :ready-only t :timeout *wait-timeout*))
                     (when (not (eql readable-sockets nil)) ;then probably the call timed out 
                       (loop for socket in readable-sockets do
                            (handler-case
                                (process-socket-wake-up socket)
                              (error (data)
                                (format t "ERROR: ~a while reading from socket ~a~%" data socket)
                                (usocket:socket-close socket)
                                (delete socket *open-sockets-list*)))))
                     (maphash
                      (lambda (key value)
                        (handler-case
                            (let ((time-difference-ms (- (get-internal-real-time) value)))
                              (cond
                                ((> time-difference-ms (* 1000 *keepalive-deletion-threshold* ))
                                 (remhash key *last-read-time-map*))
                                ((> time-difference-ms (* 1000 *keepalive-transmission-threshold*))
                                 (send-keepalive key))
                                (t nil) ; do nothing
                                ))
                          (error (data)
                            (format t "ERROR: ~a while performing keepalive actions to socket ~a~%" data key)
                            (usocket:socket-close key)
                            (delete key *open-sockets-list*))))
                      *last-read-time-map*
                      )))
               (error (data) (format t "ERROR:h ~a~%" data))
               )))
      (progn
        (format t "closing *central-server-socket*")
        (loop for socket in *open-sockets-list* do
             (usocket:socket-close socket))
        (setf *open-sockets-list* nil)
        (clrhash *last-read-time-map*)) 
      )))

;(run-server)




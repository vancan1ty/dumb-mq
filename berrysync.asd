;; Currell Berry versioned sync utility
;; starting small and building back up again

(defsystem "berrysync"
   :description "Currell Berry's versioned sync utility"
   :version "0.1"
   :author "Currell Berry <currellberry@gmail.com>"
   :depends-on (#:berryutils #:alexandria #:usocket #:bordeaux-threads #:split-sequence #:verbose #:queues #:cl-threadpool #:closer-mop)
   :in-order-to ((asdf:test-op (asdf:test-op "berrysync/tests")))
   :components (
                (:file "package")
                (:file "dumb-expect" :depends-on ("package"))
                (:file "bqueue" :depends-on ("package"))
                ;;(:file "git-helper" :depends-on ("dumb-expect" "package"))
                (:file "message" :depends-on ("dumb-expect" "package"))
                (:file "plugin" :depends-on ("message" "package"))
                (:file "berrysocket" :depends-on ("package"))
                (:file "dmq-common" :depends-on ("message" "package" "berrysocket"))
                (:file "simple-handler" :depends-on ("message" "dmq-common" "package" "plugin"))
;;                (:file "filesync-client" :depends-on ("git-helper"))
;;                (:file "filesync-server" :depends-on ("git-helper"))
                ))

(defsystem "berrysync/tests"
   :description "berrysync testing code"
   :version "0.1"
   :author "Currell Berry <currellberry@gmail.com>"
   :depends-on (#:berryutils #:alexandria #:usocket #:berrysync #:fiveam #:split-sequence #:verbose #:queues #:cl-threadpool)
   :perform (asdf:test-op (o c) (uiop:symbol-call :berrysync.tests :run-tests))
   :pathname "tests/"
   :components (
                (:file "package")
                (:file "tests" :depends-on ("package"))
                (:file "message-test" :depends-on ("tests"))
;;                (:file "berrysync-test" :depends-on ("tests"))
;;                (:file "berrysync-client-test" :depends-on ("tests"))
                ))

;;  (defparameter *tvar* (list 1 2 3 4))
;; (nconc (list 1 2) (list 3 4))
;; (append (list 1 2) (list 3 4))

;; (reverse '(1 2 3))
;; (nreverse '(1 2 3))

;; (ed "~/.sbclrc")
